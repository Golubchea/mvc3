TEMPLATE = app
#CONFIG += console c++11
qtHaveModule(widgets): QT += widgets
TARGET = mvc3
QT +=quick quickcontrols2 multimedia network positioning location svg#core gui core


QMAKE_CXXFLAGS += -arch $$QT_ARCH


HEADERS += src/OpenDialog.h
SOURCES += src/OpenDialog.cpp

HEADERS += src/documenthandler.h
SOURCES += src/documenthandler.cpp

HEADERS += src/Data.h
SOURCES += src/Data.cpp

SOURCES +=  src/main.cpp
HEADERS +=  src/tablemodel.h
SOURCES +=  src/tablemodel.cpp


##mail

HEADERS +=src/mail/emailaddress.h
HEADERS +=src/mail/mimeinlinefile.h
HEADERS +=src/mail/mimepart_p.h
HEADERS +=src/mail/server_p.h
HEADERS +=src/mail/emailaddress_p.h
HEADERS +=src/mail/mimemessage.h
HEADERS +=src/mail/mimetext.h
HEADERS +=src/mail/serverreply.h
HEADERS +=src/mail/mimeattachment.h
HEADERS +=src/mail/mimemessage_p.h
HEADERS +=src/mail/quotedprintable.h
HEADERS +=src/mail/serverreply_p.h
HEADERS +=src/mail/mimecontentformatter.h

HEADERS +=src/mail/mimemultipart.h
HEADERS +=src/mail/sender.h
HEADERS +=src/mail/smtpexports.h
HEADERS +=src/mail/mimefile.h
HEADERS +=src/mail/mimemultipart_p.h
HEADERS +=src/mail/sender_p.h
HEADERS +=src/mail/mimehtml.h
HEADERS +=src/mail/mimepart.h
HEADERS +=src/mail/server.h
HEADERS +=src/mail/SimpleMail


#SOURCES +=  src/mail/main.cpp
SOURCES +=  src/mail/emailaddress.cpp
SOURCES +=  src/mail/mimeinlinefile.cpp
SOURCES +=  src/mail/quotedprintable.cpp
SOURCES +=  src/mail/mimeattachment.cpp
SOURCES +=  src/mail/mimemessage.cpp
SOURCES +=  src/mail/sender.cpp
SOURCES +=  src/mail/mimecontentformatter.cpp
SOURCES +=  src/mail/mimemultipart.cpp
SOURCES +=  src/mail/server.cpp
SOURCES +=  src/mail/mimefile.cpp
SOURCES +=  src/mail/mimepart.cpp
SOURCES +=  src/mail/serverreply.cpp
SOURCES +=  src/mail/mimehtml.cpp
SOURCES +=  src/mail/mimetext.cpp


RESOURCES += mvc3.qrc

OTHER_FILES += \
    qml/*.qml\
    qml/select/*.qml

 
target.path = $$[QT_INSTALL_EXAMPLES]/mvc3
INSTALLS += target
ios{
CONFIG+=no_autoqmake
QMAKE_INFO_PLIST = ios/Info.plist

#BUNDLE_DATA.files += $$PWD/ios/Images.xcassets/*.png
BUNDLE_DATA.files += $$PWD/fonts/fontello.ttf
QMAKE_BUNDLE_DATA += BUNDLE_DATA

#QMAKE_ASSET_CATALOGS = $$PWD/ios/Images.xcassets
#QMAKE_ASSET_CATALOGS_APP_ICON = "AppIcon"
}

#linux
#{
#QT += printsupport
#}

#uncomment for android
android{

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml

contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
}

}









