
#ifndef OpenDialog_H
#define OpenDialog_H
#include <memory>
#include <vector>
#include <string>

#include <QObject>

class OpenDialog : public QObject
{

    Q_OBJECT
    Q_PROPERTY(QString current_path READ name WRITE setName NOTIFY nameChanged)

    Q_PROPERTY(bool isActive READ isActive WRITE set_isActive NOTIFY nameChanged)\
    Q_PROPERTY(bool isSaveActive READ isSaveActive WRITE set_isSaveActive NOTIFY nameChanged)

    Q_PROPERTY(QString file READ get_file NOTIFY nameChanged )
    Q_PROPERTY(QString dir READ get_dir WRITE set_dir NOTIFY nameChanged )

    Q_PROPERTY(QString file_render READ get_file_render NOTIFY nameChanged )
    Q_PROPERTY(QString dir_render READ get_dir_render   NOTIFY nameChanged )

    Q_PROPERTY(unsigned int file_iterator READ get_file_iterator WRITE set_file_iterator  NOTIFY dir_iteratorChanged)
    Q_PROPERTY(unsigned int dir_iterator READ get_dir_iterator WRITE set_dir_iterator  NOTIFY dir_iteratorChanged)
    Q_PROPERTY(unsigned int file_size READ get_file_size WRITE set_file_size  NOTIFY dir_iteratorChanged)
    Q_PROPERTY(unsigned int dir_size READ get_dir_size WRITE set_dir_size  NOTIFY dir_iteratorChanged)
public:
    OpenDialog(QString name="name");
    ~OpenDialog();
    // first set file and dir ints
    QString get_dir( ) const;
    QString get_file( ) const;

    QString get_dir_render( ) const;
    QString get_file_render( ) const;

    void set_vectors();


    void set_isActive(bool & b);
    bool isActive();

    void set_isSaveActive(bool & b);
    bool isSaveActive();

    QString set_dir( QString s)  ;
    //QString set_file( ) const;

    //
    unsigned int get_dir_iterator( ) const;
    void set_dir_iterator(const  unsigned int &ster);

    unsigned int get_dir_size( ) const;
    void set_dir_size(const  unsigned int &ster);
    //
    ////////////////////////////////////////////////////////////////
    unsigned int get_file_iterator( ) const;
    void set_file_iterator(const  unsigned int &ster);

    unsigned int get_file_size( ) const;
    void set_file_size(const  unsigned int &ster);
    //





    QString name() const;
    void setName(const QString &name);



signals:

    void nameChanged();
    void dir_iteratorChanged();




private:

    QString current_path;
    bool m_isActive=false;
    bool m_isSaveActive=false;

    unsigned int m_dir_iterator;
    unsigned int m_dir_size;

    unsigned int m_file_iterator;
    unsigned int m_file_size;



    //files dirs
    std::vector<QString>m_files;
    std::vector<QString>m_dirs;


};




#endif //OpenDialog_H
