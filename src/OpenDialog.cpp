#include "OpenDialog.h"
#include <QStandardPaths>
#include <QDir>
#include <iostream>
#include <qdiriterator.h>
#include <QDebug>

//999 magig number means that selected is NOT init by else classes

void OpenDialog::set_vectors()
{
    if(!m_files.empty())
        m_files.clear();
    if(!m_dirs.empty())
        m_dirs.clear();

    QDir s2=current_path;

    QDirIterator it2(s2.path(), QStringList() << "*", QDir::Files, QDirIterator::NoIteratorFlags);
    while (it2.hasNext())
    {
        QString s=it2.next();
      //  qDebug() << "file:"<<s;


        m_files.push_back( s  );

    }

    QDirIterator it(s2.path(), QStringList() << "*", QDir::Dirs, QDirIterator::NoIteratorFlags);
    while (it.hasNext())
    {
        QString s=it.next();


        if(s[s.size()-1]=='.')
        {

        }
        else
        {
            qDebug() << "dir:"<<s;
            m_dirs.push_back( s  );

        }
    }
    m_dir_size=m_dirs.size();
    m_file_size=m_files.size();
}

void OpenDialog::set_isActive(bool &b)
{
    m_isActive=b;
}

bool OpenDialog::isActive()
{
    return m_isActive;
}

void OpenDialog::set_isSaveActive(bool &b)
{
    m_isSaveActive=b;
}

bool OpenDialog::isSaveActive()
{
    return m_isSaveActive;
}



OpenDialog::OpenDialog(QString name) : m_dir_iterator(999)
{

    QDir s2;
    QStringList s;
    s=QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation);
    s2= s.at(0);

    if(!s2.cd("Houses"))
    {
        s2.mkdir("Houses");
        s2.cd("Houses");
    }

    s2.cdUp();


    //delete jpgs
    QDirIterator it2(s2.path(), QStringList() << "*", QDir::Files, QDirIterator::NoIteratorFlags);
    while (it2.hasNext())
    {
        QString s=it2.next();
        if(it2.fileInfo().completeSuffix()=="jpg" ||it2.fileInfo().completeSuffix()=="jpeg"||
                it2.fileInfo().completeSuffix()=="JPG")
        {

            qDebug() << it2.fileInfo().completeSuffix()<<"\n";
            QFile file (s);
            file.remove();
        }



    }
    s2.cd("Houses");




  //  qDebug() << "s2.path():"<<s2.path();


    current_path=s2.path();
    set_vectors();

}

OpenDialog::~OpenDialog()
{

}

QString OpenDialog::get_dir() const
{
    if(!m_dirs.empty())
    {
        return  m_dirs.at( m_dir_iterator);
    }

    else {
        return  "";
    }

}

QString OpenDialog::get_file() const
{
    if(!m_files.empty())
    {
        return  m_files.at( m_file_iterator);
    }
    else {
        return  "";
    }
}

QString OpenDialog::get_dir_render() const
{
    QString s=m_dirs.at( m_dir_iterator);


    int j = 0;

    j = s.lastIndexOf('/');


    s.remove(0, j+1);
    return s;

}

QString OpenDialog::get_file_render() const
{
    QString s=m_files.at( m_file_iterator);


    int j = 0;

    j = s.lastIndexOf('/');
    //qDebug() << "Found / tag at index position" << j;

    s.remove(0, j+1);
    return s;

}

QString OpenDialog::set_dir(QString path)
{
    current_path=path;
    set_vectors();


}



unsigned int OpenDialog::get_dir_iterator() const
{
    return  m_dir_iterator;
}

void OpenDialog::set_dir_iterator(const unsigned int &ster)
{
    m_dir_iterator=ster;

}

unsigned int OpenDialog::get_dir_size() const
{
    return  m_dir_size;
}

void OpenDialog::set_dir_size(const unsigned int &ster)
{
    m_dir_size=ster;
}

unsigned int OpenDialog::get_file_iterator() const
{
    return  m_file_iterator;
}

void OpenDialog::set_file_iterator(const unsigned int &ster)
{
    m_file_iterator=ster;

}

unsigned int OpenDialog::get_file_size() const
{
    return  m_file_size;
}

void OpenDialog::set_file_size(const unsigned int &ster)
{
    m_file_size=ster;

}


QString OpenDialog::name() const
{
    return current_path;
}

void OpenDialog::setName(const QString &name)
{
    QDir d;
    if(name=="up")
    {
        d=current_path;
        d.cdUp();
    }
    else if (name=="in")
    {
        d= m_dirs[ m_dir_iterator];
    }
    else
    {
        d=name;
    }


    current_path=d.path();
    set_vectors();

}
