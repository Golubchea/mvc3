#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <qqmlcontext.h>
#include "OpenDialog.h"
#include <qlogging.h>

//QLOGGING_H

#include <QQmlApplicationEngine>
//our classes
#include "documenthandler.h"
#include "Data.h"
#include <QFontDatabase>

#include <iostream>
#include "tablemodel.h"
int main(int argc, char *argv[])
{
    //data init
    //////////////////////////////////////////////////////////////////////////
    auto Data11=new Data("0_level");





    Data11->p_Data.push_back(new Data("Квартира без отделки"));



    Data11->p_Data[0]->p_Data.push_back(new Data("Стены") );
    Data11->p_Data[0]->p_Data[0]->p_Data.push_back(new Data("МОНОЛИТ (СНиП 3.03.01-87 п.п.\n 2.111, 2.113)") );
    Data11->p_Data[0]->p_Data[0]->p_Data.push_back(new Data("ПОЗАГРЕБЕНЬ (СНиП 3.03.01-87 п.п.\n 6.4-6.8) ") );
    Data11->p_Data[0]->p_Data[0]->p_Data.push_back(new Data("КИРПИЧНАЯ КЛАДКА (СНиП 3.03.01-87 п.п. 7.3,\n 7.4, 7.6, 7.21, 7.29, 7.90)  ") );
    Data11->p_Data[0]->p_Data[0]->p_Data.push_back(new Data("ГАЗОБЕТОН \n (СНиП 3.03.01-87 п.п. 6.4-6.8) ") );
    Data11->p_Data[0]->p_Data[0]->p_Data.push_back(new Data("Ж/б перемычки (СНиП 3.03.01-87)  ") );
    Data11->p_Data[0]->p_Data[0]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[0]->p_Data.push_back(new Data("") );


    Data11->p_Data[0]->p_Data.push_back(new Data("Пол") );
    Data11->p_Data[0]->p_Data[1]->p_Data.push_back(new Data("СТЯЖКА (СНиП 3.04.01-87) ") );
    Data11->p_Data[0]->p_Data[1]->p_Data.push_back(new Data("Устроиства перекрытий и ж/б плит \n  (СНиП 3.04.01-87)  ") );
    Data11->p_Data[0]->p_Data[1]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[1]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[1]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[1]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[1]->p_Data.push_back(new Data("") );


    Data11->p_Data[0]->p_Data.push_back(new Data("Потолок") );
    Data11->p_Data[0]->p_Data[2]->p_Data.push_back(new Data("ПЕРЕКРЫТИЯ (СНиП 3.03.01-87 п.п.\n 3.5-3.7, 3.19-3.22)  ") );
    Data11->p_Data[0]->p_Data[2]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[2]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[2]->p_Data.push_back(new Data(""));
    Data11->p_Data[0]->p_Data[2]->p_Data.push_back(new Data(""));

    Data11->p_Data[0]->p_Data[2]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[2]->p_Data.push_back(new Data("") );

    Data11->p_Data[0]->p_Data.push_back(new Data("Проемы") );
    Data11->p_Data[0]->p_Data[3]->p_Data.push_back(new Data("ПРОЁМЫ  (СНиП 3.03.01-87 ) ") );
    Data11->p_Data[0]->p_Data[3]->p_Data.push_back(new Data("Устроиство \n входной двери и \n дверной коробки \n (СНиП 3.03.01-87 ) ") );
    Data11->p_Data[0]->p_Data[3]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[3]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[3]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[3]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[3]->p_Data.push_back(new Data("") );

    Data11->p_Data[0]->p_Data.push_back(new Data("Установка оконных блоков") );
    Data11->p_Data[0]->p_Data[4]->p_Data.push_back(new Data("УСТАНОВКА ОКОННЫХ БЛОКОВ  \n (СНиП 3.03.01-87 п. 5.6)") );
    Data11->p_Data[0]->p_Data[4]->p_Data.push_back(new Data("СТЕКЛА , ПЛАСТИК \n (СНиП 3.04.01-87 п.п. 3.46, 3.47 )") );
    Data11->p_Data[0]->p_Data[4]->p_Data.push_back(new Data("Откосы (СНиП 3.03.01-87 п.п.) \n") );
    Data11->p_Data[0]->p_Data[4]->p_Data.push_back(new Data("Герметизация швов и \n откосов (СНиП 3.03.01-87 п.п.) \n "));
    Data11->p_Data[0]->p_Data[4]->p_Data.push_back(new Data("Устроиство \n подоконников\n (СНиП 3.03.01-87 п.п.) \n "));
    Data11->p_Data[0]->p_Data[4]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[4]->p_Data.push_back(new Data("") );

    Data11->p_Data[0]->p_Data.push_back(new Data("Вентканал") );
    Data11->p_Data[0]->p_Data[5]->p_Data.push_back(new Data("ВЕНТКАНАЛ (СНиП 3.02.01-87 )") );
    Data11->p_Data[0]->p_Data[5]->p_Data.push_back(new Data("ВЕНТКАНАЛ (СНиП 21-01-97 )"));
    Data11->p_Data[0]->p_Data[5]->p_Data.push_back(new Data("Установка \n рекуператорной \n решетки \n (СНиП 3.03.01-87 )"));
    Data11->p_Data[0]->p_Data[5]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[5]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[5]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[5]->p_Data.push_back(new Data("") );

    Data11->p_Data[0]->p_Data.push_back(new Data("Электрика и разводка") );
    Data11->p_Data[0]->p_Data[6]->p_Data.push_back(new Data("ЭЛЕКТРИКА И РАЗВОДКА \n(СНиП 3.05.06-85 )") );
    Data11->p_Data[0]->p_Data[6]->p_Data.push_back(new Data("Установка розеток и \n выключателей \n (СНиП 3.03.01-87 )"));
    Data11->p_Data[0]->p_Data[6]->p_Data.push_back(new Data("УСТАНОВКА РАДИАТОРА (СНиП 3.05.01-85)"));
    Data11->p_Data[0]->p_Data[6]->p_Data.push_back(new Data("Установка электрощитка \n (СНиП 3.05.06-85 )") );
    Data11->p_Data[0]->p_Data[6]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[6]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[6]->p_Data.push_back(new Data("") );

    Data11->p_Data[0]->p_Data.push_back(new Data("Сантехника") );
    Data11->p_Data[0]->p_Data[7]->p_Data.push_back(new Data("МОНТАЖ ТРУБОПРОВОДОВ \n (СНиП 3.05.01-85) ") );
    Data11->p_Data[0]->p_Data[7]->p_Data.push_back(new Data("Монтаж канализации \n (СНиП 3.05.01-85)") );
    Data11->p_Data[0]->p_Data[7]->p_Data.push_back(new Data("Монтаж \n полотенце-сушителя \n (СНиП 3.05.01-85)") );
    Data11->p_Data[0]->p_Data[7]->p_Data.push_back(new Data("Монтаж  \n счетчиков ГВС и ХВС \n  (СНиП 3.05.01-85)") );
    Data11->p_Data[0]->p_Data[7]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[7]->p_Data.push_back(new Data("") );
    Data11->p_Data[0]->p_Data[7]->p_Data.push_back(new Data("") );

    //2
    Data11->p_Data.push_back(new Data("Квартира c отделкой") );







    Data11->p_Data[1]->p_Data.push_back(new Data("Стены") );
    Data11->p_Data[1]->p_Data[0]->p_Data.push_back(new Data("ОШТУКАТУРИВАНИЕ СТЕН (СНиП 3.04.01-87)") );
    Data11->p_Data[1]->p_Data[0]->p_Data.push_back(new Data("ОКЛЕЙКА ОБОЯМИ (СНиП 3.04.01-87) ") );
    Data11->p_Data[1]->p_Data[0]->p_Data.push_back(new Data("ОКРАСКА КРАСКОЙ (СНиП 3.04.01-87) ") );
    Data11->p_Data[1]->p_Data[0]->p_Data.push_back(new Data("МОНОЛИТ (СНиП 3.03.01-87 п.п.\n 2.111, 2.113)") );
    Data11->p_Data[1]->p_Data[0]->p_Data.push_back(new Data("ПОЗАГРЕБЕНЬ (СНиП 3.03.01-87 п.п.\n 6.4-6.8) ") );
    Data11->p_Data[1]->p_Data[0]->p_Data.push_back(new Data("КИРПИЧНАЯ КЛАДКА (СНиП 3.03.01-87 п.п. 7.3,\n 7.4, 7.6, 7.21, 7.29, 7.90)  ") );
    Data11->p_Data[1]->p_Data[0]->p_Data.push_back(new Data("ГАЗОБЕТОН \n (СНиП 3.03.01-87 п.п. 6.4-6.8) ") );
    Data11->p_Data[1]->p_Data[0]->p_Data.push_back(new Data("Ж/б перемычки (СНиП 3.03.01-87)  ") );
    Data11->p_Data[1]->p_Data[0]->p_Data.push_back(new Data("Облицовочные работы \n (СНиП 3.03.01-87)  ") );

    Data11->p_Data[1]->p_Data.push_back(new Data("Пол") );
    Data11->p_Data[1]->p_Data[1]->p_Data.push_back(new Data("ЛАМИНАТ ( СНиП 3.04.01-87) ") );
    Data11->p_Data[1]->p_Data[1]->p_Data.push_back(new Data("ЛИНОЛЕУМ (СНиП 3.04.01-87)") );
    Data11->p_Data[1]->p_Data[1]->p_Data.push_back(new Data("Устроиство полов \n из плитки  \n( СНиП 3.04.01-87)") );
    Data11->p_Data[1]->p_Data[1]->p_Data.push_back(new Data("Устроиство стяжки \n( СНиП 3.04.01-87)"));
    Data11->p_Data[1]->p_Data[1]->p_Data.push_back(new Data(""));
    Data11->p_Data[1]->p_Data[1]->p_Data.push_back(new Data(""));
    Data11->p_Data[1]->p_Data[1]->p_Data.push_back(new Data(""));


    Data11->p_Data[1]->p_Data.push_back(new Data("Потолок") );
    Data11->p_Data[1]->p_Data[2]->p_Data.push_back(new Data("ПОДВЕСНОЙ ПОТОЛОК \n(СНиП 3.04.01-87 )  ") );
    Data11->p_Data[1]->p_Data[2]->p_Data.push_back(new Data("Потолок типа Армстронг \n(СНиП 3.04.01-87 ) ") );
    Data11->p_Data[1]->p_Data[2]->p_Data.push_back(new Data("Покраска потолков  \n(СНиП 3.04.01-87 ) "));
    Data11->p_Data[1]->p_Data[2]->p_Data.push_back(new Data("Оштукатуривание \nконструкции\n из гипсокартона \n(СНиП 3.04.01-87 )"));
    Data11->p_Data[1]->p_Data[2]->p_Data.push_back(new Data("Декоративные наличники , уголки, кантики \n(СНиП 3.04.01-87 ) "));
    Data11->p_Data[1]->p_Data[2]->p_Data.push_back(new Data(""));
    Data11->p_Data[1]->p_Data[2]->p_Data.push_back(new Data(""));

    Data11->p_Data[1]->p_Data.push_back(new Data("Проемы") );
    Data11->p_Data[1]->p_Data[3]->p_Data.push_back(new Data("УСТАНОВКА ДВЕРНЫХ БЛОКОВ \n(СНиП 3.03.01-87) ") );
    Data11->p_Data[1]->p_Data[3]->p_Data.push_back(new Data("УСТАНОВКА ДВЕРНОЙ КОРОБКИ \n(СНиП 3.03.01-87 )") );
    Data11->p_Data[1]->p_Data[3]->p_Data.push_back(new Data("ФУРНИТУРА \n(СНиП 3.03.01-87 )") );
    Data11->p_Data[1]->p_Data[3]->p_Data.push_back(new Data("Установка наличников \n(СНиП 3.03.01-87 )"));
    Data11->p_Data[1]->p_Data[3]->p_Data.push_back(new Data("Установка перемычек \n(СНиП 3.03.01-87 )"));
    Data11->p_Data[1]->p_Data[3]->p_Data.push_back(new Data("Устроиство \n межкомнатных  \n дверей \n(СНиП 3.03.01-87 )"));
    Data11->p_Data[1]->p_Data[3]->p_Data.push_back(new Data("Устроиство \n входной \n двери и \n дверной коробки \n(СНиП 3.03.01-87 )"));

    Data11->p_Data[1]->p_Data.push_back(new Data("Установка оконных блоков") );
    Data11->p_Data[1]->p_Data[4]->p_Data.push_back(new Data("УСТАНОВКА ОКОННЫХ БЛОКОВ  \n (СНиП 3.03.01-87 п. 5.6)") );
    Data11->p_Data[1]->p_Data[4]->p_Data.push_back(new Data("СТЕКЛА, ПЛАСТИК \n (СНиП 3.04.01-87 п.п. 3.46, 3.47 )") );
    Data11->p_Data[1]->p_Data[4]->p_Data.push_back(new Data("Устроиство\n откосов (СНиП 3.03.01-87 п.п.) \n "));
    Data11->p_Data[1]->p_Data[4]->p_Data.push_back(new Data("Устроиство \n подоконников\n (СНиП 3.03.01-87 п.п.) \n "));
    Data11->p_Data[1]->p_Data[4]->p_Data.push_back(new Data("Герметизация швов и \n откосов (СНиП 3.03.01-87 п.п.) \n "));
    Data11->p_Data[1]->p_Data[4]->p_Data.push_back(new Data(""));
    Data11->p_Data[1]->p_Data[4]->p_Data.push_back(new Data(""));


    Data11->p_Data[1]->p_Data.push_back(new Data("Вентканал") );
    Data11->p_Data[1]->p_Data[5]->p_Data.push_back(new Data("ВЕНТКАНАЛ (СНиП 3.02.01-87 )") );
    Data11->p_Data[1]->p_Data[5]->p_Data.push_back(new Data("ВЕНТКАНАЛ (СНиП 21-01-97 )"));
    Data11->p_Data[1]->p_Data[5]->p_Data.push_back(new Data("Установка \n рекуператорной \n решетки \n (СНиП 3.03.01-87 )"));
    Data11->p_Data[1]->p_Data[5]->p_Data.push_back(new Data(""));
    Data11->p_Data[1]->p_Data[5]->p_Data.push_back(new Data(""));
    Data11->p_Data[1]->p_Data[5]->p_Data.push_back(new Data(""));
    Data11->p_Data[1]->p_Data[5]->p_Data.push_back(new Data(""));


    Data11->p_Data[1]->p_Data.push_back(new Data("Электрика и разводка") );
    Data11->p_Data[1]->p_Data[6]->p_Data.push_back(new Data("ЭЛЕКТРИКА И РАЗВОДКА \n ( СНиП 3.05.06-85)") );
    Data11->p_Data[1]->p_Data[6]->p_Data.push_back(new Data("Установка \n полотенце-сушителя \n (СНиП 3.03.01-87 )"));
    Data11->p_Data[1]->p_Data[6]->p_Data.push_back(new Data("Устроиство заземления \n (СНиП 3.03.01-87 )"));
    Data11->p_Data[1]->p_Data[6]->p_Data.push_back(new Data(""));
    Data11->p_Data[1]->p_Data[6]->p_Data.push_back(new Data(""));
    Data11->p_Data[1]->p_Data[6]->p_Data.push_back(new Data(""));
    Data11->p_Data[1]->p_Data[6]->p_Data.push_back(new Data(""));


    Data11->p_Data[1]->p_Data.push_back(new Data("Сантехника") );
    Data11->p_Data[1]->p_Data[7]->p_Data.push_back(new Data("МОНТАЖ ТРУБОПРОВОДОВ  \n (СНиП 3.05.01-85 , \n СНиП 3.05.01-85 )") );
    Data11->p_Data[1]->p_Data[7]->p_Data.push_back(new Data("СЧЁТЧИКИ")  );
    Data11->p_Data[1]->p_Data[7]->p_Data.push_back(new Data("КАНАЛИЗАЦИЯ (СНиП 3.05.01-85 )")  );
    Data11->p_Data[1]->p_Data[7]->p_Data.push_back(new Data("УНИТАЗ (СНиП 3.05.01-85)")  );
    Data11->p_Data[1]->p_Data[7]->p_Data.push_back(new Data("ВАНАНЯ/ДУШЕВАЯ (СНиП 3.05.01-85 )")  );
    Data11->p_Data[1]->p_Data[7]->p_Data.push_back(new Data("СМЕСИТЕЛЬ (СНиП 3.05.01-85 )")  );
    Data11->p_Data[1]->p_Data[7]->p_Data.push_back(new Data("УМЫВАЛЬНИК (СНиП 3.05.01-85  )")  );



    Data11->set_i(0);
    Data11->set_is(Data11->p_Data.size());

    Data11->set_j(0);
    Data11->set_js(Data11->p_Data[0]->p_Data.size());

    Data11->set_k(0);
    Data11->set_ks(Data11->p_Data[0]->p_Data[0]->p_Data.size());

    //Data11->printAllInnerContainers();
    /////////////////////////////////////////////////////////////////////////





    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);




    QGuiApplication app(argc, argv);
    QFontDatabase fontDatabase;
    if(fontDatabase.addApplicationFont(":/fonts/fontello.ttf"   )==-1)
        std::cout<<"Failed to load fontello.ttf";



    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/qml/main.qml"));

    TableModel model;
    engine.rootContext()->setContextProperty("table_model", &model);



    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
                         if (!obj && url == objUrl)
                             QCoreApplication::exit(-1);
                     }, Qt::QueuedConnection);

    qmlRegisterType<DocumentHandler>("texteditor", 1, 0, "DocumentHandler");


    auto OpenDialog1=new OpenDialog("");
    engine.rootContext()->setContextProperty("OpenDialog1", OpenDialog1);
    engine.rootContext()->setContextProperty("Data11", Data11);

    engine.load(url);

    return app.exec();
}
