/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "documenthandler.h"

#include <QFile>
#include <QFileInfo>
#include <QFileSelector>
#include <QQmlFile>
#include <QQmlFileSelector>
#include <QQuickTextDocument>
#include <QTextCharFormat>
#include <QTextCodec>
#include <QTextDocument>
#include <QTextDocumentWriter>

#include <QDebug>
#include <qdebug.h>


#ifndef __APPLE__
//#include <QPrinter>
#endif





#include <iostream>
DocumentHandler::DocumentHandler(QObject *parent)
    : QObject(parent)
    , m_document(nullptr)
    , m_cursorPosition(-1)
    , m_selectionStart(0)
    , m_selectionEnd(0)
{

    std::vector<QString> names;
    QString name01=("corridor");
    QString name02="kitchen";
    QString name03=("hallway");
    QString name04=("bathroom1");
    QString name05=("bathroom2");
    QString name06=("balcony");
    QString name07=("lodgie");
    QString name08=("garage");
    QString name1=("room1");
    QString name2=("room2");
    QString name3=("room3");
    QString name4=("room4");
    QString name5=("room5");
    QString name6=("room6");
    QString name7=("room7");
    QString name8=("room8");
    QString name9=("room9");
    QString name10=("room10");
    QString name11=("room11");
    QString name12=("room12");
    QString name13=("room13");
    QString name14=("room14");
    QString name15=("room15");





    names.push_back(name01);
    names.push_back(name02);
    names.push_back(name03);
    names.push_back(name04);

    names.push_back(name05);
    names.push_back(name06);
    names.push_back(name07);
    names.push_back(name08);


    names.push_back(name1);
    names.push_back(name2);
    names.push_back(name3);
    names.push_back(name4);
    names.push_back(name5);
    names.push_back(name6);
    names.push_back(name7);
    names.push_back(name8);
    names.push_back(name9);
    names.push_back(name10);
    names.push_back(name11);
    names.push_back(name12);
    names.push_back(name13);
    names.push_back(name14);
    names.push_back(name15);




    m_previous_document= name01;
    setcurrent_document(name01);





    for(auto &i :names)
    {
        QTextDocument *t= new QTextDocument(nullptr);
        m_documents.insert({i, t});
        m_image_counter.insert({i,0});
    }

    this->setFontFamily("Times New Roman");


}


void DocumentHandler::set_document1(const QString &name)
{
    m_previous_document=m_current_document;

    m_current_document=m_previous_document;
    auto i=m_document->textDocument();
    m_documents.at(m_previous_document)->setHtml(i->toHtml());



    m_current_document=name;
    auto i2=m_document->textDocument();
    i2->setHtml(  m_documents.at(m_current_document)->toHtml());

}


void DocumentHandler::save_document1(const QString &name)
{
    m_current_document=name;
    auto i=m_document->textDocument();
    m_documents.at(m_current_document)->setHtml(i->toHtml());

}

void DocumentHandler::set_m_otdelka(const QString &name)
{
    m_otdelka=name;
}

void DocumentHandler::set_m_address(const QString &name)
{
    m_address=name;
}

void DocumentHandler::set_m_houseNum(const QString &name)
{
    m_houseNum=name;
}

void DocumentHandler::set_m_houseRoomNum(const QString &name)
{
    m_houseRoomNum=name;
}

void DocumentHandler::set_m_checkerName(const QString &name)
{
    m_checkerName=name;
}

QString DocumentHandler::getAddr() const
{
    if(m_address.isEmpty())
    {
        QString s="untitled";
        return s;
    }
    else
    {
        return m_address+m_houseNum+m_houseRoomNum;
    }

}


//void DocumentHandler::set_document1(const QString &name)
//{
//    m_current_document=name;
//    auto i=m_document->textDocument();
//    i->setHtml(  m_documents.at(m_current_document)->toHtml());

//}


//void DocumentHandler::save_document1(const QString &name)
//{
//    m_current_document=name;
//    auto i=m_document->textDocument();
//    m_documents.at(m_current_document)->setHtml(i->toHtml());

//}





DocumentHandler::~DocumentHandler()
{
    for(auto &i :m_documents)
    {
        delete  i.second;
    }
}

QString DocumentHandler::current_document() const
{
    return  m_current_document;
}

void DocumentHandler::setcurrent_document(const QString &family)
{
    m_current_document=family;
}

QQuickTextDocument *DocumentHandler::document() const
{

    //    auto i=m_document->textDocument();

    //    i->setHtml(  m_documents.at(m_current_document)->toHtml());

    //    qDebug() << "document()"<<  "";

    return m_document;
}





QTextDocument *DocumentHandler::textDocument() const
{
    if (!m_document)
        return nullptr;
    qDebug() << "set document() cursor" ;
    return m_document->textDocument();
}


void DocumentHandler::setDocument(QQuickTextDocument *document)
{
    if (document == m_document)
        return;
    qDebug() << "set document()" ;
    m_document = document;
    auto i=m_document->textDocument();
    m_documents.at(m_current_document)->setHtml(i->toHtml());


    emit documentChanged();
}

int DocumentHandler::cursorPosition() const
{
    return m_cursorPosition;
}

void DocumentHandler::setCursorPosition(int position)
{
    if (position == m_cursorPosition)
        return;

    m_cursorPosition = position;
    reset();
    emit cursorPositionChanged();
}

int DocumentHandler::selectionStart() const
{
    return m_selectionStart;
}

void DocumentHandler::setSelectionStart(int position)
{
    if (position == m_selectionStart)
        return;

    m_selectionStart = position;
    emit selectionStartChanged();
}

int DocumentHandler::selectionEnd() const
{
    return m_selectionEnd;
}

void DocumentHandler::setSelectionEnd(int position)
{
    if (position == m_selectionEnd)
        return;

    m_selectionEnd = position;
    emit selectionEndChanged();
}

QString DocumentHandler::fontFamily() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return QString();
    QTextCharFormat format = cursor.charFormat();
    return format.font().family();
}

void DocumentHandler::setFontFamily(const QString &family)
{
    QTextCharFormat format;
    format.setFontFamily(family);
    mergeFormatOnWordOrSelection(format);
    emit fontFamilyChanged();
}

QColor DocumentHandler::textColor() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return QColor(Qt::black);
    QTextCharFormat format = cursor.charFormat();
    return format.foreground().color();
}

void DocumentHandler::setTextColor(const QColor &color)
{
    QTextCharFormat format;
    format.setForeground(QBrush(color));
    mergeFormatOnWordOrSelection(format);
    emit textColorChanged();
}

Qt::Alignment DocumentHandler::alignment() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return Qt::AlignLeft;
    return textCursor().blockFormat().alignment();
}

void DocumentHandler::setAlignment(Qt::Alignment alignment)
{
    QTextBlockFormat format;
    format.setAlignment(alignment);
    QTextCursor cursor = textCursor();
    cursor.mergeBlockFormat(format);
    emit alignmentChanged();
}

QString DocumentHandler::image()
{
    return m_image;
}

bool DocumentHandler::bold() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return false;
    return textCursor().charFormat().fontWeight() == QFont::Bold;
}

void DocumentHandler::setBold(bool bold)
{
    QTextCharFormat format;
    format.setFontWeight(bold ? QFont::Bold : QFont::Normal);
    mergeFormatOnWordOrSelection(format);
    emit boldChanged();
}

bool DocumentHandler::italic() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return false;
    return textCursor().charFormat().fontItalic();
}

void DocumentHandler::setItalic(bool italic)
{
    QTextCharFormat format;
    format.setFontItalic(italic);
    mergeFormatOnWordOrSelection(format);
    emit italicChanged();
}

bool DocumentHandler::underline() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return false;
    return textCursor().charFormat().fontUnderline();
}

void DocumentHandler::setUnderline(bool underline)
{
    QTextCharFormat format;
    format.setFontUnderline(underline);
    mergeFormatOnWordOrSelection(format);
    emit underlineChanged();
}

int DocumentHandler::fontSize() const
{
    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return 0;
    QTextCharFormat format = cursor.charFormat();
    return format.font().pointSize();
}

void DocumentHandler::setFontSize(int size)
{
    if (size <= 0)
        return;

    QTextCursor cursor = textCursor();
    if (cursor.isNull())
        return;

    if (!cursor.hasSelection())
        cursor.select(QTextCursor::WordUnderCursor);

    if (cursor.charFormat().property(QTextFormat::FontPointSize).toInt() == size)
        return;

    QTextCharFormat format;
    format.setFontPointSize(size);
    mergeFormatOnWordOrSelection(format);
    emit fontSizeChanged();
}

QString DocumentHandler::fileName() const
{
    const QString filePath = QQmlFile::urlToLocalFileOrQrc(m_fileUrl);
    const QString fileName = QFileInfo(filePath).fileName();
    if (fileName.isEmpty())
    {
        return  m_current_document ;
    }
    return fileName;
}

void DocumentHandler::setfileName(const QString &name)
{
    std::cout<< " name set ="<< name.toStdString()<<std::endl;
    m_fileName=name;
}

QString DocumentHandler::fileType() const
{
    return m_fileType;
}

void DocumentHandler::setfileType(const QString &type)
{
    std::cout<< " type set ="<< type.toStdString()<<std::endl;
    m_fileType=type;
}

int DocumentHandler::fileTypeID() const
{
    return m_fileTypeID;
}

void DocumentHandler::setfileTypeID(const int &typeID)
{
    m_fileTypeID=typeID;
}

QUrl DocumentHandler::fileUrl() const
{
    return m_fileUrl;
}

void DocumentHandler::attachText(const QString &name)
{
    //QTextDocument *doc = textDocument();
    QTextCursor  cur = textCursor();
    m_text=name;
    cur.insertHtml("<br>");
    cur.insertText(name);
    cur.insertHtml("<br>");
}


QString DocumentHandler::text()
{
    return m_text;

}

void DocumentHandler::addImage(const QUrl &fileUrl)
{
    QQmlEngine *engine = qmlEngine(this);
    if (!engine) {
        qWarning() << "load() called before DocumentHandler has QQmlEngine";
        return;
    }

    const QUrl path = QQmlFileSelector::get(engine)->selector()->select(fileUrl);
    std::cout <<" load--="<<  path.toString().toStdString()<< std::endl;


    qDebug()<<"void DocumentHandler::addImage(const QUrl &fileUrl)  444\n";


    QTextDocument *doc = textDocument();
    QTextCursor  cur = textCursor();





    QImage image2(path.toLocalFile());



    doc->addResource(QTextDocument::ImageResource, path, QVariant(image2));




    // cur.insertText("Code less. Create more.");
    //add strs
    QString s1="<img src='";
    QString s2=path.toString();

    QString s3;

    if(image2.size().width()>600)
    {
        s3="' alt='photo' width='300'/>";
    }else
    {
        s3="' alt='photo'  />";
    }


    QString shtml=s1+s2+s3;

    cur.insertImage(image2);
    cur.insertHtml(shtml);

}


void DocumentHandler::attachImage(const QString &fileUrl)
{
    QQmlEngine *engine = qmlEngine(this);
    if (!engine) {
        qWarning() << "load() called before DocumentHandler has QQmlEngine";
        return;
    }

    const QUrl path = QQmlFileSelector::get(engine)->selector()->select(fileUrl);
    QPixmap pix;


    qDebug()<<"DocumentHandler::attachImage(const QString &fileUrl) 499\n";
    if(!pix.load(path.url() ,"JPG")  )
    {
        qWarning("Failed to load QPixmap");
    }

    if( pix.width()<pix.height()  )
    {
        QMatrix rm;
        rm.rotate(90);
        int pxw = pix.width(), pxh = pix.height();
        pix = pix.transformed(rm);
        pix = pix.copy((pix.width() - pxw)/2, (pix.height() - pxh)/2, pxw, pxh);
    }
    else
    {

    }




    QTextDocument *doc = textDocument();
    QTextCursor  cur = textCursor();

    //    QTextDocument * doc2=m_documents.at(m_current_document);
    //    QTextCursor  cur2=QTextCursor(doc2);
    //    if (m_selectionStart != m_selectionEnd) {
    //        cur2.setPosition(m_selectionStart);
    //        cur2.setPosition(m_selectionEnd, QTextCursor::KeepAnchor);
    //    } else {
    //        cur2.setPosition(m_cursorPosition);
    //    }

    QImage image=pix.toImage();


    QString name;
    name="mydata://image"+m_current_document+QString::number(m_image_counter.at(m_current_document))+".jpg";



    doc->addResource(QTextDocument::ImageResource, QUrl(name), QVariant(image));




    QVariant img= doc->resource(QTextDocument::ImageResource, name);
    QImage image1 = img.value<QImage>();


    QTextImageFormat imageFormat;
    imageFormat.setName(name);
    imageFormat.setQuality(100);
    // imageFormat.setWidth(pix.width());


    //640 all width
    qreal width_phone=300.0;
    imageFormat.setWidth(width_phone);
    imageFormat.setHeight(width_phone*static_cast<qreal>(pix.height())  /  (  static_cast<qreal>(pix.width())  )     );
    cur.insertHtml("<br>");
    cur.insertImage(imageFormat);
    cur.insertHtml("<br>");


    m_image_counter.at(m_current_document)++;


}


void DocumentHandler::load(const QUrl &fileUrl)
{

    if (fileUrl == m_fileUrl)
        return;

    QQmlEngine *engine = qmlEngine(this);
    if (!engine) {
        qWarning() << "load() called before DocumentHandler has QQmlEngine";
        return;
    }
    const QUrl path = QQmlFileSelector::get(engine)->selector()->select(fileUrl);
    std::cout <<" fileUrl:"<< fileUrl.path().toStdString()<<std::endl;










    QString add="file:/"+fileUrl.path();
    int k=0;
    k = add.lastIndexOf("/");
    add.resize(k);


    const QString fileName = QQmlFile::urlToLocalFileOrQrc(path);
    std::string type= QFileInfo(fileName).suffix().toStdString();
    QString name= QFileInfo(fileName).fileName() ;
    setfileName(name);
    std::cout<<type;
    if (QFile::exists(fileName))
    {
        QFile file(fileName);
        if (file.open(QFile::ReadOnly))
        {
            QByteArray data = file.readAll();

            QTextDocument *doc = textDocument();


            if (type=="txt")
            {

            }
            else if (type=="html") {

                //adding images
                QString tempstr=data;
                data.clear();
                //<img src="1.png"></img>
                qDebug() <<add;
                std::vector<int> img_idx;

                int j = 0;
                while ((j = tempstr.indexOf("<img src=", j)) != -1)
                {
                    qDebug() << "Found img src= tag at index position" << j;
                    img_idx.push_back(j);

                    ++j;
                }
                if(!img_idx.empty())
                {
                    for (unsigned int i=0;i<img_idx.size();++i)
                    {
                        tempstr.insert(img_idx[i]+10,add+'/');
                    }
                }
                qDebug() <<tempstr;
                data=tempstr.toLocal8Bit();
            }








            QTextCodec *codec = Qt::codecForHtml(data);
            QString str = codec->toUnicode(data);
            if (Qt::mightBeRichText(str)) {
                doc->setHtml(str);
            } else {
                str = QString::fromLocal8Bit(data);
                doc->setPlainText(str);
            }




            emit loaded(str);
            reset();
        }
    }

    m_fileUrl = fileUrl;
    emit fileUrlChanged();

}



#ifndef __APPLE__
#include <QtPrintSupport/QPrinter>
#endif



void DocumentHandler::saveAs(const QUrl &fileUrl)
{
    QQmlEngine *engine = qmlEngine(this);
    if (!engine) {
        qWarning() << "load() called before DocumentHandler has QQmlEngine";
        return;
    }

    std::cout <<"--1saveAs-------"<< std::endl;

    QString f =fileUrl.toLocalFile();

    auto result1= std::find(f.begin(),f.end(),".") ;
    if (result1 != f.end()) {
        std::cout << "v содержит точку " <<  result1 - f.begin() + 1  <<'\n';
        // обрезает до вида: /home/u/Документы/2.
        f.truncate(result1 - f.begin() + 1);

        std::cout << "теперь f  это: " <<  f.toStdString() <<'\n';
    }
    else
    {
        f=f+".";
    }



    if (  this->fileTypeID()== 0  )
    {
        QTextDocument *doc = textDocument();
        if (!doc)
            return;
        setfileType("txt");

        std::cout <<  "typeid="<< this->fileTypeID()<< std::endl;
        //std::cout <<"filePath="<<filePath.toStdString()<< std::endl;


        QFile file(f+fileType());
        if (!file.open(QFile::WriteOnly | QFile::Truncate | (   QFile::Text )  )   ) {
            emit error(tr("Cannot save: ") + file.errorString());
            return;
        }
        file.write( doc->toPlainText().toUtf8()   );
        file.close();

        if (fileUrl == m_fileUrl)
            return;

        m_fileUrl = fileUrl;
        emit fileUrlChanged();

    }else if (this->fileTypeID()==1)
    {
        QTextDocument *doc = textDocument();
        if (!doc)
            return;
        setfileType("html");



        std::cout <<  "typeid="<< this->fileTypeID()<< std::endl;



        QFile file(f+fileType());
        if (!file.open(QFile::WriteOnly | QFile::Truncate | (  QFile::Text))) {
            emit error(tr("Cannot save: ") + file.errorString());
            return;
        }

        QByteArray arr=doc->toHtml().toUtf8();
        QString str ="<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>";
        arr.insert(108,str);

        file.write(arr);

        file.close();



        if (fileUrl == m_fileUrl)
            return;

        m_fileUrl = fileUrl;
        emit fileUrlChanged();

    }
    else if (this->fileTypeID()==2)
    {
        setfileType("odt");

        std::cout <<  "typeid="<< this->fileTypeID()<< std::endl;
        std::cout <<  "type =" << this->fileType().toStdString()<< std::endl;





        QTextDocumentWriter writer(f+fileType());



        odtFile=f+fileType();
        // assuming some QTextDocument named 'source', return rich text as html QString object
        //        QString html = doc->toHtml();

        //        // pass substring to new QTextDocument instance
        //        QTextDocument *dest2=doc->clone();
        //        delete  dest2;


        save_document1(m_current_document);


        QTextDocument  doc;// = textDocument();
        QTextCursor cursor(&doc);
        cursor.setPosition(0);


        QString address="<div align=\"left\">Адрес:</div><b><br>"+m_address+"<br></b>";
        cursor.insertHtml(address);

        QString hkv="<div align=\"left\">Дом:</div><b><br>"+m_houseNum+"<br></b>";
        cursor.insertHtml(hkv);

        QString kv="<div align=\"left\">Квартира:</div><b><br>"+m_houseNum+"<br></b>";
        cursor.insertHtml(kv);

        QString hkvotd="<div align=\"left\">"+m_otdelka+"<br></div>";
        cursor.insertHtml(hkvotd);

        for(auto &i :m_documents)
        {
            QTextDocument * doc1 = textDocument();

            if(!i.second->isEmpty())
            {
                QString BlockName="<b>"+i.first+"<br></b>";
                cursor.insertHtml(BlockName);
            }

            for(size_t j=0;j<m_image_counter.at(i.first);++j)
            {
                QString name;
                name="mydata://image"+i.first+QString::number(j)+".jpg";






                QVariant  img  =doc1->resource(QTextDocument::ImageResource, name);




                QImage image = img.value<QImage>();






                doc.addResource(QTextDocument::ImageResource, QUrl(name), img);




                //doc->resource(QTextDocument::ImageResource, name);



                QTextImageFormat imageFormat;
                imageFormat.setName(name);
                imageFormat.setQuality(100);
                // imageFormat.setWidth(pix.width());


                //640 all width
                qreal width_phone=300.0;
                imageFormat.setWidth(width_phone);
                imageFormat.setHeight(width_phone*  image.height()  /image.width()     );
                cursor.insertImage(imageFormat);



                cursor.insertHtml("<br>");


            }

            QString s=i.second->toHtml();

            s.remove(QRegExp("<img src=[^>]*>"));

            //qDebug()<<s<<"\n";



            cursor.insertHtml(s);
        }


        QString BlockName="<table style=\"width: 992px; height: 49px;\">\r\n<tbody>\r\n<tr style=\"height: 32px;"
                          "\">\r\n<td style=\"height: 32px; width: 486px;\">Проверил:"
                          "<\/td>\r\n<td style=\"height: 32px; width: 505px;\">"+m_checkerName+
                "<\/td>\r\n<\/tr>\r\n<tr style=\"height: 32px;\">\r\n<td style=\"height: 32px; width:"
                " 486px;\">Подпись<\/td>\r\n<td style=\"height: 32px; width:"
                " 505px;\"><img src=\"mydata://images/logoPetchatb.png\" width=\"300\" height=\"300\" border=0 style=\"border:0; text-decoration:none; outline:none\"> <\/td>\r\n<\/tr>\r\n<tr style=\"height: 32px;\">\r\n<"
                "td style=\"height: 32px; width: 486px;\"><\/td>\r\n<td style=\"height: 32px; width: 505px;\">"
                "<\/td >\r\n<\/tr>\r\n<\/tbody> \r\n<\/table>";
        cursor.insertHtml(BlockName);




        QImage image1;
        if(image1.load(":/images/logoPetchatb.png"))
        {
            qDebug()<<"petchatb loaded!";
        }
        else
        {
            qDebug()<<"petchatb not loaded!";
        }



        QString name1;
        name1="mydata://images/logoPetchatb.png";

        doc.addResource(QTextDocument::ImageResource, QUrl(name1), QVariant(image1));



        QTextImageFormat imageFormat;
        imageFormat.setName(name1);
        imageFormat.setQuality(100);

        // imageFormat.setWidth(pix.width());


        //640 all width

        imageFormat.setWidth(300);
        imageFormat.setHeight(300  );
        //cursor.insertImage(imageFormat);

        //        QString BlockName2=
        //        cursor.insertHtml(BlockName2);


        bool success = writer.write(&doc);



        qDebug()<<doc.toHtml();




        m_fileUrl = fileUrl;
        emit fileUrlChanged();

    }
    else if (this->fileTypeID()==3)
    {
        setfileType("pdf");
        QTextDocument *doc = textDocument();
        if (!doc)
            return;


        QSizeF f_size(600,600);
        doc->setPageSize(f_size);

#ifndef __APPLE__
        QPrinter printer(QPrinter::HighResolution);

        printer.setOutputFormat(QPrinter::PdfFormat);
        //        printer.setFullPage(true);
        //        printer.setPaperSize(QPrinter::A4);
        printer.setOutputFileName(f+fileType());
        //   printer.set
        doc->print(&printer);
#endif










        m_fileUrl = fileUrl;
        emit fileUrlChanged();
    }





}

void DocumentHandler::set_mail(const QString &name)
{
    m_client_mail=name;
}


#include <QtCore>

#include "mail/SimpleMail"


void DocumentHandler::sendMail()
{

    using namespace SimpleMail;

    Sender smtp(QLatin1String("smtp.gmail.com"), 465, Sender::SslConnection);

    smtp.setUser(QLatin1String("homexpert60@gmail.com"));
    smtp.setPassword(QLatin1String("ZxcvbnmQwe"));

    // Create a MimeMessage

    MimeMessage message;

    EmailAddress sender(QLatin1String("homexpert60@gmail.com"), QLatin1String("HomeExpert"));
    message.setSender(sender);

    if(m_client_mail.isEmpty())
    {
        return ;
    }
    else
    {


        QByteArray ba = m_client_mail.toLatin1();


        EmailAddress to(QLatin1String(ba.data()), QLatin1String("Client"));
        message.addTo(to);

        message.setSubject(QLatin1String("HomeExpert documents"));



        MimeHtml *html=new MimeHtml;

        html->setHtml(QLatin1String("<img src='cid:image1' />"
                                    "<h2>Here is your document!</h2>"
                                    "Best wishes, HomeExpert team"

                                    ));


        // Create a MimeInlineFile object for each image
        MimeInlineFile *image1=new MimeInlineFile(new QFile(QLatin1String(":/images/Logo.png")));

        // An unique content id must be setted
        image1->setContentId(QByteArrayLiteral("image1"));
        image1->setContentType(QByteArrayLiteral("image/png"));


        message.addPart(html);
        message.addPart(image1);



        // Add an another attachment
        QFile *f=new QFile(odtFile);

        MimeAttachment *document=new MimeAttachment(f);
        document->setContentType(QByteArrayLiteral("application/vnd.oasis.opendocument.text"));
        message.addPart(document);

        // Now we can send the mail
        if (!smtp.sendMail(message)) {
            qDebug() << "Failed to send mail!" << smtp.lastError();
            //    return -3;
        }


        qDebug() << "1\n";
        f->close();


        qDebug() << "deletes\n";
        //delete document;
        //delete  f;
        //delete image1;
        //delete html;
        qDebug() << "deletes done\n";
        smtp.quit();




    }


}






void DocumentHandler::reset()
{
    emit fontFamilyChanged();
    emit alignmentChanged();
    emit boldChanged();
    emit italicChanged();
    emit underlineChanged();
    emit fontSizeChanged();
    emit textColorChanged();
}

QTextCursor DocumentHandler::textCursor() const
{
    QTextDocument *doc = textDocument();
    if (!doc)
        return QTextCursor();

    QTextCursor cursor = QTextCursor(doc);
    if (m_selectionStart != m_selectionEnd) {
        cursor.setPosition(m_selectionStart);
        cursor.setPosition(m_selectionEnd, QTextCursor::KeepAnchor);
    } else {
        cursor.setPosition(m_cursorPosition);
    }
    return cursor;
}



void DocumentHandler::mergeFormatOnWordOrSelection(const QTextCharFormat &format)
{
    QTextCursor cursor = textCursor();
    if (!cursor.hasSelection())
        cursor.select(QTextCursor::WordUnderCursor);
    cursor.mergeCharFormat(format);
}
