/****************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef DOCUMENTHANDLER_H
#define DOCUMENTHANDLER_H

#include <QFont>
#include <QObject>
#include <QTextCursor>
#include <QUrl>
#include <QDebug>
#include <qdebug.h>
QT_BEGIN_NAMESPACE
class QTextDocument;
class QQuickTextDocument;
QT_END_NAMESPACE




class DocumentHandler : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QQuickTextDocument *document READ document WRITE setDocument NOTIFY documentChanged)
    Q_PROPERTY(int cursorPosition READ cursorPosition WRITE setCursorPosition NOTIFY cursorPositionChanged)
    Q_PROPERTY(int selectionStart READ selectionStart WRITE setSelectionStart NOTIFY selectionStartChanged)
    Q_PROPERTY(int selectionEnd READ selectionEnd WRITE setSelectionEnd NOTIFY selectionEndChanged)

    Q_PROPERTY(QColor textColor READ textColor WRITE setTextColor NOTIFY textColorChanged)
    Q_PROPERTY(QString fontFamily READ fontFamily WRITE setFontFamily NOTIFY fontFamilyChanged)
    Q_PROPERTY(Qt::Alignment alignment READ alignment WRITE setAlignment NOTIFY alignmentChanged)

    Q_PROPERTY(bool bold READ bold WRITE setBold NOTIFY boldChanged)
    Q_PROPERTY(bool italic READ italic WRITE setItalic NOTIFY italicChanged)
    Q_PROPERTY(bool underline READ underline WRITE setUnderline NOTIFY underlineChanged)

    Q_PROPERTY(int fontSize READ fontSize WRITE setFontSize NOTIFY fontSizeChanged)

    Q_PROPERTY(QString fileName READ fileName WRITE setfileName NOTIFY fileUrlChanged)
    Q_PROPERTY(QString fileType READ fileType WRITE setfileType NOTIFY fileUrlChanged)
    Q_PROPERTY(int fileTypeID READ fileTypeID WRITE setfileTypeID NOTIFY fileUrlChanged)

    Q_PROPERTY(QUrl fileUrl READ fileUrl NOTIFY fileUrlChanged)



    Q_PROPERTY(QString m_current_document READ fileName WRITE setfileName NOTIFY fileUrlChanged)


    Q_PROPERTY(QString image READ image WRITE addImage NOTIFY documentChanged)
    //  Q_PROPERTY(QString image WRITE attachImage NOTIFY documentChanged)
    Q_PROPERTY(QString m_text READ text WRITE attachText NOTIFY documentChanged)

public:
    //unsigned int m_image_counter;

    explicit DocumentHandler(QObject *parent = nullptr);
    ~DocumentHandler( );




    QString current_document() const;
    void setcurrent_document(const QString &family);



    QQuickTextDocument *document() const;
    void setDocument(QQuickTextDocument *document);

    int cursorPosition() const;
    void setCursorPosition(int position);

    int selectionStart() const;
    void setSelectionStart(int position);

    int selectionEnd() const;
    void setSelectionEnd(int position);

    QString fontFamily() const;
    void setFontFamily(const QString &family);

    QColor textColor() const;
    void setTextColor(const QColor &color);

    Qt::Alignment alignment() const;
    void setAlignment(Qt::Alignment alignment);
    QString text();
    QString image();

    bool bold() const;
    void setBold(bool bold);

    bool italic() const;
    void setItalic(bool italic);

    bool underline() const;
    void setUnderline(bool underline);

    int fontSize() const;
    void setFontSize(int size);



    QString fileName() const;
    void setfileName(const QString &name);
    QString fileType() const;
    void setfileType(const QString &type);


    int fileTypeID() const;
    void setfileTypeID(const int &typeID);


    QUrl fileUrl() const;

public Q_SLOTS:
    void attachText(const QString &name);
    void attachImage(const QString &name);
    void addImage(const QUrl &fileUrl);
    void load(const QUrl &fileUrl);
    void saveAs(const QUrl &fileUrl);
    void set_mail(const QString &name);

    void sendMail();

    void set_document1(const QString &name);
    void save_document1(const QString &name);

    void set_m_otdelka(const QString &name);
    void set_m_address(const QString &name);
    void set_m_houseNum(const QString &name);
    void set_m_houseRoomNum(const QString &name);
    void set_m_checkerName(const QString &name);
QString getAddr() const;


Q_SIGNALS:
    void documentChanged();
    void cursorPositionChanged();
    void selectionStartChanged();
    void selectionEndChanged();

    void fontFamilyChanged();
    void textColorChanged();
    void alignmentChanged();

    void boldChanged();
    void italicChanged();
    void underlineChanged();

    void fontSizeChanged();

    void textChanged();
    void fileUrlChanged();

    void loaded(const QString &text);
    void error(const QString &message);

private:
    void reset();
    QTextCursor  textCursor() const;
    QTextDocument *textDocument() const;
    void mergeFormatOnWordOrSelection(const QTextCharFormat &format);





    QQuickTextDocument *m_document;

    //selector
    QString m_current_document;
    QString m_previous_document;
    std::map <QString,QTextDocument *> m_documents;
    std::map <QString,unsigned int > m_image_counter;




    //house maganement
    QString m_client_mail;

    QString m_otdelka;
    QString m_address;
    QString m_houseNum;
    QString m_houseRoomNum;
    QString m_checkerName;


    int m_cursorPosition;
    int m_selectionStart;
    int m_selectionEnd;

    QFont m_font;
    int m_fontSize;
    QUrl m_fileUrl;
    QString m_fileName;
    QString m_fileType;


     QString odtFile;

    QString m_text;
    QString m_image;

    int m_fileTypeID;
};

#endif // DOCUMENTHANDLER_H
