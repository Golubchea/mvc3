
#ifndef Data_H
#define Data_H
#include <memory>
#include <vector>
#include <string>

#include <QObject>

class Data : public QObject
{

    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString image1 READ image1 WRITE setimage1 NOTIFY image1Changed)
    //iterators for init and etc
    Q_PROPERTY(unsigned int i  READ i WRITE set_i NOTIFY iChanged)
    Q_PROPERTY(unsigned int j  READ j WRITE set_j NOTIFY jChanged)
    Q_PROPERTY(unsigned int k  READ k WRITE set_k NOTIFY kChanged)
    //sizes
    Q_PROPERTY(unsigned int is  READ is WRITE set_is NOTIFY sizeChanged)
    Q_PROPERTY(unsigned int js  READ js WRITE set_js NOTIFY sizeChanged)
    Q_PROPERTY(unsigned int ks  READ ks WRITE set_ks NOTIFY sizeChanged)

    //select
    Q_PROPERTY(unsigned int select_i  READ selecti WRITE set_selecti NOTIFY select_lv_Changed)
    Q_PROPERTY(unsigned int select_j  READ selectj WRITE set_selectj NOTIFY select_lv_Changed)
    Q_PROPERTY(unsigned int select_k  READ selectk WRITE set_selectk NOTIFY select_lv_Changed)



    Q_PROPERTY(QString level_3 READ level_3 WRITE setlevel_3 NOTIFY level3Changed)
    Q_PROPERTY(QString level_2 READ level_2 WRITE setlevel_2 NOTIFY level2Changed)
    Q_PROPERTY(QString level_1 READ level_1 WRITE setlevel_1 NOTIFY level1Changed)


    Q_PROPERTY(QString res_string READ res_string  )


public:
    Data(QString name="name");
    ~Data();



    QString name() const;
    void setName(const QString &name);

    void print();
    void printAllInnerContainers();

    QString image1( ) const;
    void setimage1(const  QString &ster);
    unsigned int i();
    unsigned int j();
    unsigned int k();
    void set_i(unsigned int i);
    void set_j(unsigned int j);
    void set_k(unsigned int k);


    //size of tree mat
    unsigned int is();
    unsigned int js();
    unsigned int ks();
    void set_is(unsigned int is);
    void set_js(unsigned int js);
    void set_ks(unsigned int ks);


    //when all 3 ijk trees set use ths
    QString level_3() const;
    void setlevel_3(const QString &name);

    QString level_2() const;
    void setlevel_2(const QString &name);


    QString level_1() const;
    void setlevel_1( const QString &name);




    unsigned int selecti();
    unsigned int selectj();
    unsigned int selectk();
    void set_selecti(unsigned int is);
    void set_selectj(unsigned int js);
    void set_selectk(unsigned int ks);


    QString res_string ();
    //main data
    std::vector<  Data *  > p_Data;


signals:

    void dir_iteratorChanged();
    void dir_sizeChanged();

    void nameChanged(QString name);
    void image1Changed(QString name);
    void iChanged(unsigned int i);
    void jChanged(unsigned int j);
    void kChanged(unsigned int k);

    void sizeChanged();


    void level3Changed(QString name);
    void level2Changed(QString name);
    void level1Changed(QString name);


    void select_lv_Changed();


private:




    QString m_image_paths;
    QString m_name;
    //iterators
    unsigned int m_i;
    unsigned int m_j;
    unsigned int m_k;

    //sizes
    unsigned int m_is;
    unsigned int m_js;
    unsigned int m_ks;

    //selected levels
    unsigned int m_select_lv1;
    unsigned int m_select_lv2;
    unsigned int m_select_lv3;



};




#endif
