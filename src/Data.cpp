#include <src/Data.h>


#include <iostream>


//999 magig number means that selected is NOT init by else classes
Data::Data(QString name) : m_select_lv1(999), m_select_lv2(999), m_select_lv3(999)
{
    QStringList s;


    m_name=name;

}





QString Data::name() const
{
    return m_name;
}

void Data::setName(const QString &name)
{
    m_name=name;
    emit(nameChanged(name));
}

void Data::print()
{
    unsigned int i=0;
    for (i=0;i<p_Data.size();++i)
    {
        std::cout<<p_Data[i]->m_name.toStdString()<< std::endl;
    }

}

void Data::printAllInnerContainers()
{
    unsigned int i=0;
    unsigned int j=0;
    unsigned int k=0;

    std::cout<<this->m_name.toStdString()<<"+++"<< std::endl;
    //level1
    if(!p_Data.empty())
    {

        for (i=0;i<p_Data.size();++i)
        {
            std::cout<<" "<<p_Data[i]->m_name.toStdString()<< std::endl;
            //level2
            if(!p_Data[i]->p_Data.empty())
            {
                for (j=0;j<p_Data[i]->p_Data.size();++j)
                {
                    std::cout<<"   "<< p_Data[i]->p_Data[j]->m_name.toStdString()  << std::endl;



                    //level3
                    if(!p_Data[i]->p_Data[j]->p_Data.empty())
                    {

                        for (k=0;k<p_Data[k]->p_Data.size();++k)
                        {
                            std::cout<<"    "<< p_Data[i]->p_Data[j]->p_Data[k]->m_name.toStdString()  << std::endl;
                        }
                    }
                }
            }




        }
    }

}

Data::~Data()
{
    p_Data.clear();
}

unsigned int Data::i()
{
    return m_i;
}

unsigned int Data::j()
{
    return m_j;
}

unsigned int Data::k()
{
    return m_k;
}

void Data::set_i(unsigned int i)
{
    m_i=i;
    emit(iChanged(i));
}

void Data::set_j(unsigned int j)
{
    m_j=j;
    emit(jChanged(j));
}

void Data::set_k(unsigned int k)
{
    m_k=k;
    emit(kChanged(k));
}
//sizes
/////////////////


unsigned int Data::is()
{

    return m_is;
}

unsigned int Data::js()
{
    return m_js;
}

unsigned int Data::ks()
{
    return m_ks;
}

void Data::set_is(unsigned int is)
{
    m_is=is;
    emit(sizeChanged());
}

void Data::set_js(unsigned int js)
{
    m_js=js;
    emit(sizeChanged());
}

void Data::set_ks(unsigned int ks)
{
    m_ks=ks;
    emit(sizeChanged());
}


///////////////////////////////////////////////////////////////////////////////

unsigned int Data::selecti()
{
    return m_select_lv1;
}

unsigned int Data::selectj()
{
    return m_select_lv2;
}

unsigned int Data::selectk()
{
    return m_select_lv3;
}

void Data::set_selecti(unsigned int is)
{
    m_select_lv1=is;
    emit(select_lv_Changed());
}

void Data::set_selectj(unsigned int js)
{
    m_select_lv2=js;
    emit(select_lv_Changed());
}

void Data::set_selectk(unsigned int ks)
{
    m_select_lv3=ks;
    emit(select_lv_Changed());
}

QString Data::res_string()
{
    if(m_select_lv1!=999)
    {
        QString s1= p_Data.at(m_select_lv1)->name();

        if(m_select_lv2!=999)
        {
            QString s2= p_Data.at(m_select_lv1)->p_Data.at(m_select_lv2)->name();


            if(m_select_lv3!=999)
            {
                QString s3= p_Data.at(m_select_lv1)->p_Data.at(m_select_lv2)->p_Data.at(m_select_lv3)->name();
                return QString( "\n"+s2+"\n"+s3);
            }
            else
            {
                return QString( "\n"+s2 );
            }


        }
        else
        {
            return QString( s1 );
        }
    }
    else {
        return QString( "необходимо выбрать из списка");
    }


}

QString Data::image1() const
{
    return m_image_paths;
}

void Data::setimage1(const QString &ster)
{
    m_image_paths=ster;
    emit(image1Changed(ster));
}


///////////////////////////////////////////////////////////////////////////////


QString Data::level_3() const
{
//    std::cout<< "---------------------------" << std::endl;
//    std::cout<< "i="<<m_i << std::endl;
//    std::cout<< "j="<<m_j << std::endl;
//    std::cout<< "k="<<m_k << std::endl;
//    std::cout<< "---------------------------" << std::endl;
    if(p_Data.at(m_i)->p_Data.empty())
    {
        return  QString("no i size");
    }
    if(p_Data.at(m_i)->p_Data.at(m_j)->p_Data.empty())
    {
        return  QString("no j size");
    }
    else {

        return p_Data.at(m_i)->p_Data.at(m_j)->p_Data.at(m_k)->name();
    }
}

void Data::setlevel_3(const QString &name)
{



    p_Data.at(m_i)->p_Data.at(m_j)->p_Data.at(m_k)->setName(name);
    emit(level3Changed(name) )  ;
}




QString Data::level_2() const
{
//    std::cout<< "---------------------------" << std::endl;
//    std::cout<< "i="<<m_i << std::endl;
//    std::cout<< "j="<<m_j << std::endl;
//    std::cout<< "---------------------------" << std::endl;
//    std::cout<< "k="<<m_k << std::endl;
//    std::cout<< "---------------------------" << std::endl;
    if(p_Data.at(m_i)->p_Data.empty())
    {
        return  QString("no i size");
    }

    else
    {
        return p_Data.at(m_i)->p_Data.at(m_j)->name();

    }
}

void Data::setlevel_2(const QString &name)
{

    p_Data.at(m_i)->p_Data.at(m_j)->setName(name);
    emit(level2Changed(name) )  ;
}


QString Data::level_1() const
{
    //,j(),k()
    return p_Data.at(m_i)->name();
}

void Data::setlevel_1(const QString &name)
{

    p_Data.at(m_i)->setName(name);
    emit(level1Changed(name) )  ;
}

