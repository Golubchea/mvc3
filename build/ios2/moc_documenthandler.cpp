/****************************************************************************
** Meta object code from reading C++ file 'documenthandler.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../src/documenthandler.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'documenthandler.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_DocumentHandler_t {
    QByteArrayData data[55];
    char stringdata0[662];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DocumentHandler_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DocumentHandler_t qt_meta_stringdata_DocumentHandler = {
    {
QT_MOC_LITERAL(0, 0, 15), // "DocumentHandler"
QT_MOC_LITERAL(1, 16, 15), // "documentChanged"
QT_MOC_LITERAL(2, 32, 0), // ""
QT_MOC_LITERAL(3, 33, 21), // "cursorPositionChanged"
QT_MOC_LITERAL(4, 55, 21), // "selectionStartChanged"
QT_MOC_LITERAL(5, 77, 19), // "selectionEndChanged"
QT_MOC_LITERAL(6, 97, 17), // "fontFamilyChanged"
QT_MOC_LITERAL(7, 115, 16), // "textColorChanged"
QT_MOC_LITERAL(8, 132, 16), // "alignmentChanged"
QT_MOC_LITERAL(9, 149, 11), // "boldChanged"
QT_MOC_LITERAL(10, 161, 13), // "italicChanged"
QT_MOC_LITERAL(11, 175, 16), // "underlineChanged"
QT_MOC_LITERAL(12, 192, 15), // "fontSizeChanged"
QT_MOC_LITERAL(13, 208, 11), // "textChanged"
QT_MOC_LITERAL(14, 220, 14), // "fileUrlChanged"
QT_MOC_LITERAL(15, 235, 6), // "loaded"
QT_MOC_LITERAL(16, 242, 4), // "text"
QT_MOC_LITERAL(17, 247, 5), // "error"
QT_MOC_LITERAL(18, 253, 7), // "message"
QT_MOC_LITERAL(19, 261, 10), // "attachText"
QT_MOC_LITERAL(20, 272, 4), // "name"
QT_MOC_LITERAL(21, 277, 11), // "attachImage"
QT_MOC_LITERAL(22, 289, 8), // "addImage"
QT_MOC_LITERAL(23, 298, 7), // "fileUrl"
QT_MOC_LITERAL(24, 306, 4), // "load"
QT_MOC_LITERAL(25, 311, 6), // "saveAs"
QT_MOC_LITERAL(26, 318, 8), // "set_mail"
QT_MOC_LITERAL(27, 327, 8), // "sendMail"
QT_MOC_LITERAL(28, 336, 13), // "set_document1"
QT_MOC_LITERAL(29, 350, 14), // "save_document1"
QT_MOC_LITERAL(30, 365, 13), // "set_m_otdelka"
QT_MOC_LITERAL(31, 379, 13), // "set_m_address"
QT_MOC_LITERAL(32, 393, 14), // "set_m_houseNum"
QT_MOC_LITERAL(33, 408, 18), // "set_m_houseRoomNum"
QT_MOC_LITERAL(34, 427, 17), // "set_m_checkerName"
QT_MOC_LITERAL(35, 445, 7), // "getAddr"
QT_MOC_LITERAL(36, 453, 8), // "document"
QT_MOC_LITERAL(37, 462, 19), // "QQuickTextDocument*"
QT_MOC_LITERAL(38, 482, 14), // "cursorPosition"
QT_MOC_LITERAL(39, 497, 14), // "selectionStart"
QT_MOC_LITERAL(40, 512, 12), // "selectionEnd"
QT_MOC_LITERAL(41, 525, 9), // "textColor"
QT_MOC_LITERAL(42, 535, 10), // "fontFamily"
QT_MOC_LITERAL(43, 546, 9), // "alignment"
QT_MOC_LITERAL(44, 556, 13), // "Qt::Alignment"
QT_MOC_LITERAL(45, 570, 4), // "bold"
QT_MOC_LITERAL(46, 575, 6), // "italic"
QT_MOC_LITERAL(47, 582, 9), // "underline"
QT_MOC_LITERAL(48, 592, 8), // "fontSize"
QT_MOC_LITERAL(49, 601, 8), // "fileName"
QT_MOC_LITERAL(50, 610, 8), // "fileType"
QT_MOC_LITERAL(51, 619, 10), // "fileTypeID"
QT_MOC_LITERAL(52, 630, 18), // "m_current_document"
QT_MOC_LITERAL(53, 649, 5), // "image"
QT_MOC_LITERAL(54, 655, 6) // "m_text"

    },
    "DocumentHandler\0documentChanged\0\0"
    "cursorPositionChanged\0selectionStartChanged\0"
    "selectionEndChanged\0fontFamilyChanged\0"
    "textColorChanged\0alignmentChanged\0"
    "boldChanged\0italicChanged\0underlineChanged\0"
    "fontSizeChanged\0textChanged\0fileUrlChanged\0"
    "loaded\0text\0error\0message\0attachText\0"
    "name\0attachImage\0addImage\0fileUrl\0"
    "load\0saveAs\0set_mail\0sendMail\0"
    "set_document1\0save_document1\0set_m_otdelka\0"
    "set_m_address\0set_m_houseNum\0"
    "set_m_houseRoomNum\0set_m_checkerName\0"
    "getAddr\0document\0QQuickTextDocument*\0"
    "cursorPosition\0selectionStart\0"
    "selectionEnd\0textColor\0fontFamily\0"
    "alignment\0Qt::Alignment\0bold\0italic\0"
    "underline\0fontSize\0fileName\0fileType\0"
    "fileTypeID\0m_current_document\0image\0"
    "m_text"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DocumentHandler[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      30,   14, // methods
      18,  224, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      15,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  164,    2, 0x06 /* Public */,
       3,    0,  165,    2, 0x06 /* Public */,
       4,    0,  166,    2, 0x06 /* Public */,
       5,    0,  167,    2, 0x06 /* Public */,
       6,    0,  168,    2, 0x06 /* Public */,
       7,    0,  169,    2, 0x06 /* Public */,
       8,    0,  170,    2, 0x06 /* Public */,
       9,    0,  171,    2, 0x06 /* Public */,
      10,    0,  172,    2, 0x06 /* Public */,
      11,    0,  173,    2, 0x06 /* Public */,
      12,    0,  174,    2, 0x06 /* Public */,
      13,    0,  175,    2, 0x06 /* Public */,
      14,    0,  176,    2, 0x06 /* Public */,
      15,    1,  177,    2, 0x06 /* Public */,
      17,    1,  180,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      19,    1,  183,    2, 0x0a /* Public */,
      21,    1,  186,    2, 0x0a /* Public */,
      22,    1,  189,    2, 0x0a /* Public */,
      24,    1,  192,    2, 0x0a /* Public */,
      25,    1,  195,    2, 0x0a /* Public */,
      26,    1,  198,    2, 0x0a /* Public */,
      27,    0,  201,    2, 0x0a /* Public */,
      28,    1,  202,    2, 0x0a /* Public */,
      29,    1,  205,    2, 0x0a /* Public */,
      30,    1,  208,    2, 0x0a /* Public */,
      31,    1,  211,    2, 0x0a /* Public */,
      32,    1,  214,    2, 0x0a /* Public */,
      33,    1,  217,    2, 0x0a /* Public */,
      34,    1,  220,    2, 0x0a /* Public */,
      35,    0,  223,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   16,
    QMetaType::Void, QMetaType::QString,   18,

 // slots: parameters
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void, QMetaType::QUrl,   23,
    QMetaType::Void, QMetaType::QUrl,   23,
    QMetaType::Void, QMetaType::QUrl,   23,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::Void, QMetaType::QString,   20,
    QMetaType::QString,

 // properties: name, type, flags
      36, 0x80000000 | 37, 0x0049510b,
      38, QMetaType::Int, 0x00495103,
      39, QMetaType::Int, 0x00495103,
      40, QMetaType::Int, 0x00495103,
      41, QMetaType::QColor, 0x00495103,
      42, QMetaType::QString, 0x00495103,
      43, 0x80000000 | 44, 0x0049510b,
      45, QMetaType::Bool, 0x00495103,
      46, QMetaType::Bool, 0x00495103,
      47, QMetaType::Bool, 0x00495103,
      48, QMetaType::Int, 0x00495103,
      49, QMetaType::QString, 0x00495003,
      50, QMetaType::QString, 0x00495003,
      51, QMetaType::Int, 0x00495003,
      23, QMetaType::QUrl, 0x00495001,
      52, QMetaType::QString, 0x00495003,
      53, QMetaType::QString, 0x00495003,
      54, QMetaType::QString, 0x00495003,

 // properties: notify_signal_id
       0,
       1,
       2,
       3,
       5,
       4,
       6,
       7,
       8,
       9,
      10,
      12,
      12,
      12,
      12,
      12,
       0,
       0,

       0        // eod
};

void DocumentHandler::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DocumentHandler *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->documentChanged(); break;
        case 1: _t->cursorPositionChanged(); break;
        case 2: _t->selectionStartChanged(); break;
        case 3: _t->selectionEndChanged(); break;
        case 4: _t->fontFamilyChanged(); break;
        case 5: _t->textColorChanged(); break;
        case 6: _t->alignmentChanged(); break;
        case 7: _t->boldChanged(); break;
        case 8: _t->italicChanged(); break;
        case 9: _t->underlineChanged(); break;
        case 10: _t->fontSizeChanged(); break;
        case 11: _t->textChanged(); break;
        case 12: _t->fileUrlChanged(); break;
        case 13: _t->loaded((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 14: _t->error((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 15: _t->attachText((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 16: _t->attachImage((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 17: _t->addImage((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 18: _t->load((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 19: _t->saveAs((*reinterpret_cast< const QUrl(*)>(_a[1]))); break;
        case 20: _t->set_mail((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 21: _t->sendMail(); break;
        case 22: _t->set_document1((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 23: _t->save_document1((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 24: _t->set_m_otdelka((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 25: _t->set_m_address((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 26: _t->set_m_houseNum((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 27: _t->set_m_houseRoomNum((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 28: _t->set_m_checkerName((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 29: { QString _r = _t->getAddr();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (DocumentHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DocumentHandler::documentChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (DocumentHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DocumentHandler::cursorPositionChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (DocumentHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DocumentHandler::selectionStartChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (DocumentHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DocumentHandler::selectionEndChanged)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (DocumentHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DocumentHandler::fontFamilyChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (DocumentHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DocumentHandler::textColorChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (DocumentHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DocumentHandler::alignmentChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (DocumentHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DocumentHandler::boldChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (DocumentHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DocumentHandler::italicChanged)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (DocumentHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DocumentHandler::underlineChanged)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (DocumentHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DocumentHandler::fontSizeChanged)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (DocumentHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DocumentHandler::textChanged)) {
                *result = 11;
                return;
            }
        }
        {
            using _t = void (DocumentHandler::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DocumentHandler::fileUrlChanged)) {
                *result = 12;
                return;
            }
        }
        {
            using _t = void (DocumentHandler::*)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DocumentHandler::loaded)) {
                *result = 13;
                return;
            }
        }
        {
            using _t = void (DocumentHandler::*)(const QString & );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DocumentHandler::error)) {
                *result = 14;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<DocumentHandler *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QQuickTextDocument**>(_v) = _t->document(); break;
        case 1: *reinterpret_cast< int*>(_v) = _t->cursorPosition(); break;
        case 2: *reinterpret_cast< int*>(_v) = _t->selectionStart(); break;
        case 3: *reinterpret_cast< int*>(_v) = _t->selectionEnd(); break;
        case 4: *reinterpret_cast< QColor*>(_v) = _t->textColor(); break;
        case 5: *reinterpret_cast< QString*>(_v) = _t->fontFamily(); break;
        case 6: *reinterpret_cast< Qt::Alignment*>(_v) = _t->alignment(); break;
        case 7: *reinterpret_cast< bool*>(_v) = _t->bold(); break;
        case 8: *reinterpret_cast< bool*>(_v) = _t->italic(); break;
        case 9: *reinterpret_cast< bool*>(_v) = _t->underline(); break;
        case 10: *reinterpret_cast< int*>(_v) = _t->fontSize(); break;
        case 11: *reinterpret_cast< QString*>(_v) = _t->fileName(); break;
        case 12: *reinterpret_cast< QString*>(_v) = _t->fileType(); break;
        case 13: *reinterpret_cast< int*>(_v) = _t->fileTypeID(); break;
        case 14: *reinterpret_cast< QUrl*>(_v) = _t->fileUrl(); break;
        case 15: *reinterpret_cast< QString*>(_v) = _t->fileName(); break;
        case 16: *reinterpret_cast< QString*>(_v) = _t->image(); break;
        case 17: *reinterpret_cast< QString*>(_v) = _t->text(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<DocumentHandler *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setDocument(*reinterpret_cast< QQuickTextDocument**>(_v)); break;
        case 1: _t->setCursorPosition(*reinterpret_cast< int*>(_v)); break;
        case 2: _t->setSelectionStart(*reinterpret_cast< int*>(_v)); break;
        case 3: _t->setSelectionEnd(*reinterpret_cast< int*>(_v)); break;
        case 4: _t->setTextColor(*reinterpret_cast< QColor*>(_v)); break;
        case 5: _t->setFontFamily(*reinterpret_cast< QString*>(_v)); break;
        case 6: _t->setAlignment(*reinterpret_cast< Qt::Alignment*>(_v)); break;
        case 7: _t->setBold(*reinterpret_cast< bool*>(_v)); break;
        case 8: _t->setItalic(*reinterpret_cast< bool*>(_v)); break;
        case 9: _t->setUnderline(*reinterpret_cast< bool*>(_v)); break;
        case 10: _t->setFontSize(*reinterpret_cast< int*>(_v)); break;
        case 11: _t->setfileName(*reinterpret_cast< QString*>(_v)); break;
        case 12: _t->setfileType(*reinterpret_cast< QString*>(_v)); break;
        case 13: _t->setfileTypeID(*reinterpret_cast< int*>(_v)); break;
        case 15: _t->setfileName(*reinterpret_cast< QString*>(_v)); break;
        case 16: _t->addImage(*reinterpret_cast< QString*>(_v)); break;
        case 17: _t->attachText(*reinterpret_cast< QString*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject DocumentHandler::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_DocumentHandler.data,
    qt_meta_data_DocumentHandler,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *DocumentHandler::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DocumentHandler::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_DocumentHandler.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int DocumentHandler::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 30)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 30;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 30)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 30;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 18;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 18;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 18;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 18;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 18;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void DocumentHandler::documentChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void DocumentHandler::cursorPositionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void DocumentHandler::selectionStartChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void DocumentHandler::selectionEndChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 3, nullptr);
}

// SIGNAL 4
void DocumentHandler::fontFamilyChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 4, nullptr);
}

// SIGNAL 5
void DocumentHandler::textColorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 5, nullptr);
}

// SIGNAL 6
void DocumentHandler::alignmentChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 6, nullptr);
}

// SIGNAL 7
void DocumentHandler::boldChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void DocumentHandler::italicChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 8, nullptr);
}

// SIGNAL 9
void DocumentHandler::underlineChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 9, nullptr);
}

// SIGNAL 10
void DocumentHandler::fontSizeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 10, nullptr);
}

// SIGNAL 11
void DocumentHandler::textChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}

// SIGNAL 12
void DocumentHandler::fileUrlChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 12, nullptr);
}

// SIGNAL 13
void DocumentHandler::loaded(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 13, _a);
}

// SIGNAL 14
void DocumentHandler::error(const QString & _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 14, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
