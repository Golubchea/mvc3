/****************************************************************************
** Meta object code from reading C++ file 'OpenDialog.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../src/OpenDialog.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'OpenDialog.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_OpenDialog_t {
    QByteArrayData data[15];
    char stringdata0[157];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_OpenDialog_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_OpenDialog_t qt_meta_stringdata_OpenDialog = {
    {
QT_MOC_LITERAL(0, 0, 10), // "OpenDialog"
QT_MOC_LITERAL(1, 11, 11), // "nameChanged"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 19), // "dir_iteratorChanged"
QT_MOC_LITERAL(4, 44, 12), // "current_path"
QT_MOC_LITERAL(5, 57, 8), // "isActive"
QT_MOC_LITERAL(6, 66, 12), // "isSaveActive"
QT_MOC_LITERAL(7, 79, 4), // "file"
QT_MOC_LITERAL(8, 84, 3), // "dir"
QT_MOC_LITERAL(9, 88, 11), // "file_render"
QT_MOC_LITERAL(10, 100, 10), // "dir_render"
QT_MOC_LITERAL(11, 111, 13), // "file_iterator"
QT_MOC_LITERAL(12, 125, 12), // "dir_iterator"
QT_MOC_LITERAL(13, 138, 9), // "file_size"
QT_MOC_LITERAL(14, 148, 8) // "dir_size"

    },
    "OpenDialog\0nameChanged\0\0dir_iteratorChanged\0"
    "current_path\0isActive\0isSaveActive\0"
    "file\0dir\0file_render\0dir_render\0"
    "file_iterator\0dir_iterator\0file_size\0"
    "dir_size"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_OpenDialog[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       2,   14, // methods
      11,   26, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   24,    2, 0x06 /* Public */,
       3,    0,   25,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,

 // properties: name, type, flags
       4, QMetaType::QString, 0x00495003,
       5, QMetaType::Bool, 0x00495003,
       6, QMetaType::Bool, 0x00495003,
       7, QMetaType::QString, 0x00495001,
       8, QMetaType::QString, 0x00495003,
       9, QMetaType::QString, 0x00495001,
      10, QMetaType::QString, 0x00495001,
      11, QMetaType::UInt, 0x00495003,
      12, QMetaType::UInt, 0x00495003,
      13, QMetaType::UInt, 0x00495003,
      14, QMetaType::UInt, 0x00495003,

 // properties: notify_signal_id
       0,
       0,
       0,
       0,
       0,
       0,
       0,
       1,
       1,
       1,
       1,

       0        // eod
};

void OpenDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<OpenDialog *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->nameChanged(); break;
        case 1: _t->dir_iteratorChanged(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (OpenDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&OpenDialog::nameChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (OpenDialog::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&OpenDialog::dir_iteratorChanged)) {
                *result = 1;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<OpenDialog *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->name(); break;
        case 1: *reinterpret_cast< bool*>(_v) = _t->isActive(); break;
        case 2: *reinterpret_cast< bool*>(_v) = _t->isSaveActive(); break;
        case 3: *reinterpret_cast< QString*>(_v) = _t->get_file(); break;
        case 4: *reinterpret_cast< QString*>(_v) = _t->get_dir(); break;
        case 5: *reinterpret_cast< QString*>(_v) = _t->get_file_render(); break;
        case 6: *reinterpret_cast< QString*>(_v) = _t->get_dir_render(); break;
        case 7: *reinterpret_cast< uint*>(_v) = _t->get_file_iterator(); break;
        case 8: *reinterpret_cast< uint*>(_v) = _t->get_dir_iterator(); break;
        case 9: *reinterpret_cast< uint*>(_v) = _t->get_file_size(); break;
        case 10: *reinterpret_cast< uint*>(_v) = _t->get_dir_size(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<OpenDialog *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setName(*reinterpret_cast< QString*>(_v)); break;
        case 1: _t->set_isActive(*reinterpret_cast< bool*>(_v)); break;
        case 2: _t->set_isSaveActive(*reinterpret_cast< bool*>(_v)); break;
        case 4: _t->set_dir(*reinterpret_cast< QString*>(_v)); break;
        case 7: _t->set_file_iterator(*reinterpret_cast< uint*>(_v)); break;
        case 8: _t->set_dir_iterator(*reinterpret_cast< uint*>(_v)); break;
        case 9: _t->set_file_size(*reinterpret_cast< uint*>(_v)); break;
        case 10: _t->set_dir_size(*reinterpret_cast< uint*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject OpenDialog::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_OpenDialog.data,
    qt_meta_data_OpenDialog,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *OpenDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *OpenDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_OpenDialog.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int OpenDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 2)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 2;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 2)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 2;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 11;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 11;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void OpenDialog::nameChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void OpenDialog::dir_iteratorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
