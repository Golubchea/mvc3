/****************************************************************************
** Meta object code from reading C++ file 'Data.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../src/Data.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Data.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Data_t {
    QByteArrayData data[29];
    char stringdata0[256];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Data_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Data_t qt_meta_stringdata_Data = {
    {
QT_MOC_LITERAL(0, 0, 4), // "Data"
QT_MOC_LITERAL(1, 5, 19), // "dir_iteratorChanged"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 15), // "dir_sizeChanged"
QT_MOC_LITERAL(4, 42, 11), // "nameChanged"
QT_MOC_LITERAL(5, 54, 4), // "name"
QT_MOC_LITERAL(6, 59, 13), // "image1Changed"
QT_MOC_LITERAL(7, 73, 8), // "iChanged"
QT_MOC_LITERAL(8, 82, 1), // "i"
QT_MOC_LITERAL(9, 84, 8), // "jChanged"
QT_MOC_LITERAL(10, 93, 1), // "j"
QT_MOC_LITERAL(11, 95, 8), // "kChanged"
QT_MOC_LITERAL(12, 104, 1), // "k"
QT_MOC_LITERAL(13, 106, 11), // "sizeChanged"
QT_MOC_LITERAL(14, 118, 13), // "level3Changed"
QT_MOC_LITERAL(15, 132, 13), // "level2Changed"
QT_MOC_LITERAL(16, 146, 13), // "level1Changed"
QT_MOC_LITERAL(17, 160, 17), // "select_lv_Changed"
QT_MOC_LITERAL(18, 178, 6), // "image1"
QT_MOC_LITERAL(19, 185, 2), // "is"
QT_MOC_LITERAL(20, 188, 2), // "js"
QT_MOC_LITERAL(21, 191, 2), // "ks"
QT_MOC_LITERAL(22, 194, 8), // "select_i"
QT_MOC_LITERAL(23, 203, 8), // "select_j"
QT_MOC_LITERAL(24, 212, 8), // "select_k"
QT_MOC_LITERAL(25, 221, 7), // "level_3"
QT_MOC_LITERAL(26, 229, 7), // "level_2"
QT_MOC_LITERAL(27, 237, 7), // "level_1"
QT_MOC_LITERAL(28, 245, 10) // "res_string"

    },
    "Data\0dir_iteratorChanged\0\0dir_sizeChanged\0"
    "nameChanged\0name\0image1Changed\0iChanged\0"
    "i\0jChanged\0j\0kChanged\0k\0sizeChanged\0"
    "level3Changed\0level2Changed\0level1Changed\0"
    "select_lv_Changed\0image1\0is\0js\0ks\0"
    "select_i\0select_j\0select_k\0level_3\0"
    "level_2\0level_1\0res_string"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Data[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
      15,  102, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      12,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   74,    2, 0x06 /* Public */,
       3,    0,   75,    2, 0x06 /* Public */,
       4,    1,   76,    2, 0x06 /* Public */,
       6,    1,   79,    2, 0x06 /* Public */,
       7,    1,   82,    2, 0x06 /* Public */,
       9,    1,   85,    2, 0x06 /* Public */,
      11,    1,   88,    2, 0x06 /* Public */,
      13,    0,   91,    2, 0x06 /* Public */,
      14,    1,   92,    2, 0x06 /* Public */,
      15,    1,   95,    2, 0x06 /* Public */,
      16,    1,   98,    2, 0x06 /* Public */,
      17,    0,  101,    2, 0x06 /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void, QMetaType::UInt,    8,
    QMetaType::Void, QMetaType::UInt,   10,
    QMetaType::Void, QMetaType::UInt,   12,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void, QMetaType::QString,    5,
    QMetaType::Void,

 // properties: name, type, flags
       5, QMetaType::QString, 0x00495103,
      18, QMetaType::QString, 0x00495003,
       8, QMetaType::UInt, 0x00495003,
      10, QMetaType::UInt, 0x00495003,
      12, QMetaType::UInt, 0x00495003,
      19, QMetaType::UInt, 0x00495003,
      20, QMetaType::UInt, 0x00495003,
      21, QMetaType::UInt, 0x00495003,
      22, QMetaType::UInt, 0x00495003,
      23, QMetaType::UInt, 0x00495003,
      24, QMetaType::UInt, 0x00495003,
      25, QMetaType::QString, 0x00495003,
      26, QMetaType::QString, 0x00495003,
      27, QMetaType::QString, 0x00495003,
      28, QMetaType::QString, 0x00095001,

 // properties: notify_signal_id
       2,
       3,
       4,
       5,
       6,
       7,
       7,
       7,
      11,
      11,
      11,
       8,
       9,
      10,
       0,

       0        // eod
};

void Data::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Data *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->dir_iteratorChanged(); break;
        case 1: _t->dir_sizeChanged(); break;
        case 2: _t->nameChanged((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: _t->image1Changed((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->iChanged((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 5: _t->jChanged((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 6: _t->kChanged((*reinterpret_cast< uint(*)>(_a[1]))); break;
        case 7: _t->sizeChanged(); break;
        case 8: _t->level3Changed((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 9: _t->level2Changed((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 10: _t->level1Changed((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 11: _t->select_lv_Changed(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (Data::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Data::dir_iteratorChanged)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (Data::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Data::dir_sizeChanged)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (Data::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Data::nameChanged)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (Data::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Data::image1Changed)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (Data::*)(unsigned int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Data::iChanged)) {
                *result = 4;
                return;
            }
        }
        {
            using _t = void (Data::*)(unsigned int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Data::jChanged)) {
                *result = 5;
                return;
            }
        }
        {
            using _t = void (Data::*)(unsigned int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Data::kChanged)) {
                *result = 6;
                return;
            }
        }
        {
            using _t = void (Data::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Data::sizeChanged)) {
                *result = 7;
                return;
            }
        }
        {
            using _t = void (Data::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Data::level3Changed)) {
                *result = 8;
                return;
            }
        }
        {
            using _t = void (Data::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Data::level2Changed)) {
                *result = 9;
                return;
            }
        }
        {
            using _t = void (Data::*)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Data::level1Changed)) {
                *result = 10;
                return;
            }
        }
        {
            using _t = void (Data::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Data::select_lv_Changed)) {
                *result = 11;
                return;
            }
        }
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        auto *_t = static_cast<Data *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< QString*>(_v) = _t->name(); break;
        case 1: *reinterpret_cast< QString*>(_v) = _t->image1(); break;
        case 2: *reinterpret_cast< uint*>(_v) = _t->i(); break;
        case 3: *reinterpret_cast< uint*>(_v) = _t->j(); break;
        case 4: *reinterpret_cast< uint*>(_v) = _t->k(); break;
        case 5: *reinterpret_cast< uint*>(_v) = _t->is(); break;
        case 6: *reinterpret_cast< uint*>(_v) = _t->js(); break;
        case 7: *reinterpret_cast< uint*>(_v) = _t->ks(); break;
        case 8: *reinterpret_cast< uint*>(_v) = _t->selecti(); break;
        case 9: *reinterpret_cast< uint*>(_v) = _t->selectj(); break;
        case 10: *reinterpret_cast< uint*>(_v) = _t->selectk(); break;
        case 11: *reinterpret_cast< QString*>(_v) = _t->level_3(); break;
        case 12: *reinterpret_cast< QString*>(_v) = _t->level_2(); break;
        case 13: *reinterpret_cast< QString*>(_v) = _t->level_1(); break;
        case 14: *reinterpret_cast< QString*>(_v) = _t->res_string(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        auto *_t = static_cast<Data *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setName(*reinterpret_cast< QString*>(_v)); break;
        case 1: _t->setimage1(*reinterpret_cast< QString*>(_v)); break;
        case 2: _t->set_i(*reinterpret_cast< uint*>(_v)); break;
        case 3: _t->set_j(*reinterpret_cast< uint*>(_v)); break;
        case 4: _t->set_k(*reinterpret_cast< uint*>(_v)); break;
        case 5: _t->set_is(*reinterpret_cast< uint*>(_v)); break;
        case 6: _t->set_js(*reinterpret_cast< uint*>(_v)); break;
        case 7: _t->set_ks(*reinterpret_cast< uint*>(_v)); break;
        case 8: _t->set_selecti(*reinterpret_cast< uint*>(_v)); break;
        case 9: _t->set_selectj(*reinterpret_cast< uint*>(_v)); break;
        case 10: _t->set_selectk(*reinterpret_cast< uint*>(_v)); break;
        case 11: _t->setlevel_3(*reinterpret_cast< QString*>(_v)); break;
        case 12: _t->setlevel_2(*reinterpret_cast< QString*>(_v)); break;
        case 13: _t->setlevel_1(*reinterpret_cast< QString*>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

QT_INIT_METAOBJECT const QMetaObject Data::staticMetaObject = { {
    QMetaObject::SuperData::link<QObject::staticMetaObject>(),
    qt_meta_stringdata_Data.data,
    qt_meta_data_Data,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Data::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Data::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Data.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Data::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 12;
    }
#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 15;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 15;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void Data::dir_iteratorChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void Data::dir_sizeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void Data::nameChanged(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Data::image1Changed(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Data::iChanged(unsigned int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void Data::jChanged(unsigned int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void Data::kChanged(unsigned int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 6, _a);
}

// SIGNAL 7
void Data::sizeChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 7, nullptr);
}

// SIGNAL 8
void Data::level3Changed(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 8, _a);
}

// SIGNAL 9
void Data::level2Changed(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 9, _a);
}

// SIGNAL 10
void Data::level1Changed(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 10, _a);
}

// SIGNAL 11
void Data::select_lv_Changed()
{
    QMetaObject::activate(this, &staticMetaObject, 11, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
