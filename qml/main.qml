import QtQuick 2.12
import QtQuick.Controls 2.5

import QtQuick.Controls.Material 2.12
import texteditor 1.0
ApplicationWindow {
    id: window
    visible: true
    width:  640/2
    height: 1136/2
    title: qsTr("Stack")



    Item {
        id: scroll
        property var value:  textArea.cursorPosition


        TextArea {
            id: textArea
            textFormat: Qt.RichText
            wrapMode: TextArea.Wrap
            //height: window.height-1
            width:20
            selectByMouse: true
            // visible: false
            persistentSelection: true
            // Different styles have different padding and background
            // decorations, but since this editor is almost taking up the
            // entire window, we don't need them.


            Material.accent: Material.Red
        }

    }





    DocumentHandler
    {
        id: document
        document:  textArea.textDocument
        cursorPosition:  textArea.cursorPosition

        selectionStart:  textArea.selectionStart
        selectionEnd:  textArea.selectionEnd
        // textColor: TODO
        //Component.onCompleted: document.load("qrc:/texteditor.html")
        onLoaded:
        {

        }
    }





    Component.onCompleted:
    {
        addImage_button.visible=false
        addsnip_button.visible=false

        textArea.visible=false;
    }








    header: RoundButton {
        // contentHeight: toolButton.implicitHeight
        id:m
        background: Rectangle {
            id:back_main
            implicitWidth: 100
            implicitHeight: 20
            visible: false
            opacity: 0.4
            //color: Material.Lime
            Material.background: Material.Indigo

        }



        Button {
            id: addImage_button
            font.family: "fontello"
            text: "Фот" // icon-pencil
            width: 70
            height: width
            // Don't want to use anchors for the y position, because it will anchor
            // to the footer, leaving a large vertical gap.
            y:  window.height-45
            x:  window.width -70
            //anchors.right: parent.right
            //anchors.margins: 12
            visible:   true
            highlighted: true

            Material.accent: Material.Red
            onClicked: {

            }
        }

        Button {
            id: addsnip_button
            font.family: "fontello"
            text: "СНИП" // icon-pencil
            width: 70
            height: width
            // Don't want to use anchors for the y position, because it will anchor
            // to the footer, leaving a large vertical gap.
            y:  window.height-45
            x: window.width -140
            //anchors.right: parent.right
            //anchors.margins:  70
            visible:   true
            highlighted: true

            Material.accent: Material.Red
            onClicked: {

            }
        }



        RoundButton {
            id: backButton
            font.family: "fontello"
            text: "<-" // icon-pencil
            width: 60
            height: width
            // Don't want to use anchors for the y position, because it will anchor
            // to the footer, leaving a large vertical gap.
            y:   window.height -70-60
            x:   parent.width -40
            //            anchors.right: parent.right
            //            anchors.margins: 12
            visible:  false
            highlighted: true

            Material.accent: Material.Cyan




        }


        RoundButton {
            id: newButton
            font.family: "fontello"
            text: "+" // icon-pencil
            width: 60
            height: width
            // Don't want to use anchors for the y position, because it will anchor
            // to the footer, leaving a large vertical gap.
            x: parent.width -40
            y: window.height -70
            //            anchors.right: parent.right
            //            anchors.margins: 12
            visible:  ( (stackView.depth > 1)&&(OpenDialog1.isActive===true)  )? true : false
            highlighted: true

            Material.accent: Material.Blue
            onClicked: {

                stackView.pop()
                stackView.push("texteditor.qml")
            }
        }


        Button {
            id: saveButton
            font.family: "fontello"
            text: "Сохр" // icon-pencil
            width: 70
            height: width
            // Don't want to use anchors for the y position, because it will anchor
            // to the footer, leaving a large vertical gap.
            x: window.width-65
            y:  -24
            //            anchors.right: parent.right
            //            anchors.margins: 12
            visible: ( (stackView.depth > 1)&&(OpenDialog1.isSaveActive===true)  )? true : false
            highlighted: true

            Material.accent: Material.Red

            onClicked:
            {

            }

        }

        Button {
            id: sendMailButton
            font.family: "fontello"
            text: "email" // icon-pencil
            width: 70
            height: width
            // Don't want to use anchors for the y position, because it will anchor
            // to the footer, leaving a large vertical gap.
            x: window.width-135
            y:  -24
            //            anchors.right: parent.right
            //            anchors.margins: 12
            visible: ( (stackView.depth > 1)&&(OpenDialog1.isSaveActive===true)  )? true : false
            highlighted: true

            Material.accent: Material.Red

            onClicked:
            {
                document.fileTypeID=2
                document.saveAs("file:/"+OpenDialog1.current_path+"/"+document.getAddr()+"."+"odt")
                document.sendMail();
            }

        }





        Button {
            id: editButton
            font.family: "fontello"
            text: "Ред" // icon-pencil
            width: 70
            height: width
            // Don't want to use anchors for the y position, because it will anchor
            // to the footer, leaving a large vertical gap.
            y: window.height-46
            x: -5
            highlighted: true
            visible: false
            Material.accent: Material.Red

            //            anchors.right: parent.right
            //            anchors.margins: 12

            onClicked:
            {
                textArea.readOnly = false
                // Force focus on the text area so the cursor and footer show up.
                textArea.forceActiveFocus()
                //textArea.persistentSelection=false
                editButton.visible=false;
                okButton.visible=true;


                addImage_button.visible=true

                addsnip_button.visible=true
            }
        }

        Button {
            id: okButton
            font.family: "fontello"
            text: "OK" // icon-pencil
            width: 70
            height: width
            // Don't want to use anchors for the y position, because it will anchor
            // to the footer, leaving a large vertical gap.
            y: window.height-46
            x: -5
            highlighted: true

            Material.accent: Material.Red
            visible: false
            //            anchors.right: parent.right
            //            anchors.margins: 12

            onClicked: {
                textArea.readOnly = true
                //textArea.persistentSelection=false

                // Force focus on the text area so the cursor and footer show up.
                editButton.visible=true;
                okButton.visible=false;

                addImage_button.visible=false

                addsnip_button.visible=false
                // textArea.forceActiveFocus()
            }
        }







        Image
        {
            id: toolButton

            x:10
            y:5

            width: 40
            height: 40


            fillMode: Image.PreserveAspectFit
            Material.accent: "white"
            //font.pixelSize: Qt.application.font.pixelSize * 1.6
            Material.background: "white"
            //anchors.centerIn: parent
            //                    sourceSize.height: button.background.height - 6
            //                    height: sourceSize.height
            source: "qrc:/images/MenuIcon@3x.png"
            MouseArea {
                id: toolButton1
                anchors.fill: parent

                onClicked: {
                    drawer.open()
                }
            }


        }



        Label
        {
            color: "red"
            id:main_label
            //anchors.fill: parent
            //anchors.centerIn: parent
            y:15
            x:70
            text: ""
            //            y:15
            //            x:60
            opacity: 60
            //anchors.centerIn: parent.left
        }






    }

    Drawer {
        id: drawer
        width: window.width * 0.44
        height: window.height

        Column {
            anchors.fill: parent

            //            ItemDelegate
            //            {
            //                text: qsTr("Открыть")
            //                width: parent.width
            //                onClicked:
            //                {

            //                    stackView.push("openDialog.qml")
            //                    drawer.close()
            //                }
            //            }


            ItemDelegate
            {
                id:homeform1
                property int idx: 15
                visible: true
                text: qsTr("Главная")
                width: parent.width
                onClicked:
                {
                    stackView.pop()
                    stackView.push("HomeForm.qml")
                    drawer.close()
                }
            }

            ItemDelegate
            {
                id:kitchen
                visible: false
                text: qsTr("Кухня")
                width: parent.width
                onClicked:
                {

                    document.set_document1("kitchen")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }


            ItemDelegate
            {
                id:hallway
                visible: false
                text: qsTr("Прихожая")
                width: parent.width
                onClicked:
                {


                    document.set_document1("hallway")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }
            ItemDelegate
            {
                id:corridor
                visible: false
                text: qsTr("Коридор")
                width: parent.width
                onClicked:
                {
                    document.set_document1("corridor")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }

            ItemDelegate
            {
                text: qsTr("С/у")
                id:bathroom1
                visible: false
                width: parent.width
                onClicked:
                {
                    document.set_document1("bathroom1")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }

            ItemDelegate
            {
                id:bathroom2
                visible: false
                text: qsTr("Ванная")
                width: parent.width
                onClicked:
                {
                    document.set_document1("bathroom2")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }

            ItemDelegate
            {
                text: qsTr("Балкон")
                id:balcony
                visible: false
                width: parent.width
                onClicked:
                {



                    document.set_document1("balcony")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }


            ItemDelegate
            {
                text: qsTr("Лоджия")
                width: parent.width
                id:lodgie
                visible: false
                onClicked:
                {
                    document.set_document1("lodgie")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }

            ItemDelegate
            {
                text: qsTr("Гараж")
                width: parent.width
                id:garage
                visible: false
                onClicked:
                {
                    document.set_document1("garage")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }

            ItemDelegate
            {
                text: qsTr("Комната1")
                property int idx: 1
                width: parent.width
                id:room1
                visible: false
                onClicked:
                {
                    document.set_document1("room1")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }

            ItemDelegate
            {
                text: qsTr("Комната2")
                property int idx: 2
                width: parent.width
                id:room2
                visible: false
                onClicked:
                {
                    document.set_document1("room2")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }
            ItemDelegate
            {
                text: qsTr("Комната3")
                property int idx: 3
                width: parent.width
                id:room3
                visible: false
                onClicked:
                {
                    document.set_document1("room3")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }
            ItemDelegate
            {
                text: qsTr("Комната4")
                width: parent.width
                property int idx: 4
                id:room4
                visible: false
                onClicked:
                {
                    document.set_document1("room4")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }
            ItemDelegate
            {
                text: qsTr("Комната5")
                width: parent.width
                property int idx: 5
                id:room5
                visible: false
                onClicked:
                {
                    document.set_document1("room5")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }
            ItemDelegate
            {
                id:room6
                property int idx: 6
                visible: false
                text: qsTr("Комната6")
                width: parent.width
                onClicked:
                {
                    document.set_document1("room6")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }
            ItemDelegate
            {
                id:room7
                property int idx: 7
                visible: false
                text: qsTr("Комната7")
                width: parent.width
                onClicked:
                {
                    document.set_document1("room7")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }
            ItemDelegate
            {
                id:room8
                visible: false
                property int idx: 8
                text: qsTr("Комната8")
                width: parent.width
                onClicked:
                {
                    document.set_document1("room8")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }
            ItemDelegate
            {
                id:room9
                property int idx: 9
                visible: false
                text: qsTr("Комната9")
                width: parent.width
                onClicked:
                {
                    document.set_document1("room9")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }
            ItemDelegate
            {
                id:room10
                visible: false
                property int idx: 10
                text: qsTr("Комната10")
                width: parent.width
                onClicked:
                {
                    document.set_document1("room10")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }
            ItemDelegate
            {
                id:room11
                visible: false
                property int idx: 11
                text: qsTr("Комната11")
                width: parent.width
                onClicked:
                {
                    document.set_document1("room11")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }
            ItemDelegate
            {
                id:room12
                property int idx: 12
                visible: false
                text: qsTr("Комната12")
                width: parent.width
                onClicked:
                {
                    document.set_document1("room12")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }
            ItemDelegate
            {
                id:room13
                visible: false
                property int idx: 13
                text: qsTr("Комната13")
                width: parent.width
                onClicked:
                {
                    document.set_document1("room13")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }
            ItemDelegate
            {
                id:room14
                property int idx: 14
                visible: false
                text: qsTr("Комната14")
                width: parent.width
                onClicked:
                {
                    document.set_document1("room14")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }
            ItemDelegate
            {
                id:room15
                property int idx: 15
                visible: false
                text: qsTr("Комната15")
                width: parent.width
                onClicked:
                {
                    document.set_document1("room15")
                    stackView.push("texteditor.qml")
                    drawer.close()
                }
            }




        }
    }

    StackView {
        id: stackView
        initialItem: "HomeForm.qml"


        anchors.fill: parent
    }
}
