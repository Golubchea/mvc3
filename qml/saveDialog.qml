
//import QtQuick 2.6
//import QtQuick.Layouts 1.0
//import QtQuick.Controls 2.1
//import QtQuick.Controls.Material 2.1




import QtQuick 2.12

import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.12
import QtQuick.Window 2.0
import Qt.labs.platform 1.0




Flickable
{
    id: flickable
    contentHeight: menu.height




    Action {
        id: saveDialogAction
        text: qsTr("&saveDialog")
        icon.name: "saveDialog"
        //on
        onTriggered:
        {
            document.fileTypeID=save_type.currentIndex
            //document.saveAs("file://home/u/Документы/Houses/вавам/dfs.txt")

            console.log("file:/"+OpenDialog1.current_path+"/"+save_field.text+"."+save_type.currentText)
            document.saveAs("file:/"+OpenDialog1.current_path+"/"+save_field.text+"."+save_type.currentText)
            //file_name.text=  document.fileName

            messageDialog.visible=true

        }


    }


    Action {
        id: saveDialogBackAction
        text: qsTr("&saveDialogBackAction")
        icon.name: "saveDialogBackAction"
        //on
        onTriggered:
        {
            OpenDialog1.current_path="up"
            stackView.pop()
            stackView.push("saveDialog.qml")
        }


    }


    Component.onCompleted:
    {
        OpenDialog1.isSaveActive=true
        OpenDialog1.isActive=false
        main_label.text=OpenDialog1.current_path

        textArea.visible=false;
        backButton.visible=true;
        addImage_button.visible=false
        addsnip_button.visible=false


        saveButton.visible=true;

        saveButton.action=saveDialogAction;
        backButton.action=saveDialogBackAction;

        editButton.visible=false;
        okButton.visible=false;


    }

    Column
    {

        id: menu
        spacing: 20
        width: parent.width



        Label {
            id: messageDialog
            visible: false
            Layout.fillWidth: true
            text: "Сохранено..."

        }
        Label
        {
            Layout.fillWidth: true
            Layout.alignment :Qt.AlignCenter
            id:file_label
            text: qsTr("Введите имя файла"+"\nдля дальнейшего\nсохранения")

        }

        ColumnLayout
        {
            spacing: 20
            anchors.leftMargin: 0
            width: parent.width

            Layout.alignment :Qt.AlignLeft




            ComboBox
            {
                Layout.fillWidth: true
                id:save_type
                model: ["txt", "html", "odt","pdf"]

            }

            TextField
            {
                Layout.fillWidth: true
                id: save_field
                placeholderText: "квартира1"


            }




            Repeater
            {
                id:r1
                model:OpenDialog1.dir_size//size
                Button
                {
                    Layout.fillWidth: true
                    id:d
                    Material.background: Material.DeepOrange
                    Text
                    {
                        id: d_pic
                        y:5
                        x:5
                        font.pixelSize:32
                        color: "#ffffff"
                        text: qsTr("\ue806")
                    }

                    Text
                    {
                        id: d_text
                        y:14
                        x:5+32+5
                        color: "#ffffff"
                        text: qsTr("\ue808")
                    }


                    Component.onCompleted:
                    {
                        OpenDialog1.dir_iterator=index
                        d_text.text=OpenDialog1.dir_render
                    }

                    onClicked:
                    {
                        OpenDialog1.dir_iterator=index
                        OpenDialog1.current_path="in"
                        stackView.pop()
                        stackView.push("saveDialog.qml")
                    }
                }






            }








            Repeater
            {
                id:r2
                model:OpenDialog1.file_size//size
                Button
                {
                    Layout.fillWidth: true
                    id:d2
                    Material.background:  Material.Green
                    Text {
                        id: d2_pic
                        y:5
                        x:5
                        font.pixelSize:32
                        color: "#ffffff"
                        text: qsTr("\uf0f6")
                    }

                    Text {
                        id: d2_text
                        y:14
                        x:5+32+5
                        color: "#ffffff"
                        text: qsTr("\ue808")
                    }


                    Component.onCompleted:
                    {
                        OpenDialog1.file_iterator=index
                        d2_text.text=OpenDialog1.file_render
                    }

                    onClicked:
                    {
                        //document.load(OpenDialog1.dir)
                    }
                }


            }




        }



    }

    RoundButton {
        id: newButton
        font.family: "fontello"
        text: "\ue803" // icon-pencil
        width: 70
        height: width
        // Don't want to use anchors for the y position, because it will anchor
        // to the footer, leaving a large vertical gap.
        x: parent.width -90
        y:  15
//            anchors.right: parent.right
//            anchors.margins: 12
        visible: true
        highlighted: true

        Material.accent: Material.Blue
        onClicked: {

            stackView.pop()
            stackView.push("texteditor.qml")
        }
    }


    ScrollIndicator.vertical: ScrollIndicator { }

}

