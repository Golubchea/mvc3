
import QtQuick 2.0
import QtMultimedia 5.4
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.0

import QtQuick.Controls.Material 2.1


import texteditor 1.0


Flickable {
    id: menu

    Material.accent: "#ed1925"

    Image {
        id: img1name
        anchors.fill : parent
                fillMode: Image.PreserveAspectFit

        visible: false
    }


    RoundButton {

        id: append_image
        font.family: "fontello"
        text: "\ue807" // icon-pencil
        width: 60
        height: width
        // Don't want to use anchors for the y position, because it will anchor
        // to the footer, leaving a large vertical gap.

        x:window.width/2.0-30 +30.0
        y:   500
        //anchors.right: parent.right
        //anchors.margins: 12
        visible:  false
        highlighted: true

        Material.accent: Material.Blue
        onClicked:
        {
            console.log(camera.imageCapture.capturedImagePath)
            Data11.image1=camera.imageCapture.capturedImagePath

            console.log(Data11.image1)
            // Data11.add_image_path(saveLabel.text)
            // Data11
            //this entry for text editor
            stackView.pop()
            stackView.push("texteditor.qml")
            document.attachImage(Data11.image1)


            textArea.readOnly = false
            // Force focus on the text area so the cursor and footer show up.
            textArea.forceActiveFocus()
            //textArea.persistentSelection=false
            editButton.visible=false;
            okButton.visible=true;


            addImage_button.visible=true

            addsnip_button.visible=true


        }
    }

    RoundButton {

        id: cancel_image
        font.family: "fontello"
        text: "\ue801" // icon-pencil
        width: 60
        height: width
        // Don't want to use anchors for the y position, because it will anchor
        // to the footer, leaving a large vertical gap.

        x:window.width/2.0-30 -30
        y:  500
        //anchors.right: parent.right
        //anchors.margins: 12
        visible:  false
        highlighted: true

        Material.accent: "red"
        onClicked:
        {
            camera.start();
            cap_image.visible=true
            append_image.visible=false
            this.visible=false

             img1name.visible=false

        }
    }




    Component.onCompleted:
    {
        addImage_button.visible=false
        addsnip_button.visible=false
        saveButton.visible=false;
        textArea.visible=false;
        m.visible=false;
sendMailButton.visible=false;

        editButton.visible=false;
        okButton.visible=false;
         img1name.visible=false;

    }


    Camera
    {
        imageCapture {
            onImageCaptured: {

            }
        }
        id: camera
        captureMode: Camera.CaptureStillImage
        //viewfinder.resolution: "5000x3000"
        imageCapture {
            onImageCaptured:
            {
                Data11.image1=camera.imageCapture.capturedImagePath;
                img1name.source=preview;
                img1name.visible=true;
            }
        }
    }
    VideoOutput
    {
        id: viewfinder
        visible: true

        x: 0
        y: 0
        width: parent.width
        height:parent.height

        source: camera
        autoOrientation: true
    }

    RoundButton {
        id: cap_image
        font.family: "fontello"
        text: "\ue802" // icon-pencil
        width: 60
        height: width
        // Don't want to use anchors for the y position, because it will anchor
        // to the footer, leaving a large vertical gap.

        x:window.width/2.0-30.0
        y:  500
        //anchors.right: parent.right
        //anchors.margins: 12
        visible: true
        highlighted: true

        Material.accent: "red"
        onClicked:
        {
            // m.visible=true;

            camera.imageCapture.capture();
            camera.stop();
            this.visible=false
            append_image.visible=true;
            cancel_image.visible=true;








        }


    }

    ScrollIndicator.vertical: ScrollIndicator { }


}



