
//import QtQuick 2.6
//import QtQuick.Layouts 1.0
//import QtQuick.Controls 2.1
//import QtQuick.Controls.Material 2.1




import QtQuick 2.12

import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.12
import QtQuick.Window 2.0
import Qt.labs.platform 1.0
import texteditor 1.0



Flickable
{
    id: flickable
    contentHeight: menu.height


    Action {
        id: loadDialogBackAction
        text: qsTr("&loadBackDialog")
        icon.name: "loadBackDialog"
        //on
        onTriggered:
        {

            OpenDialog1.current_path="up"
            stackView.pop()
            stackView.push("openDialog.qml")
        }
    }


    Action {
        id: backed
        text: qsTr("&dloadBackDialog")
        icon.name: "dloadBackDialog"
        //on
        onTriggered:
        {


            stackView.pop()


        }
    }


    Component.onCompleted:
    {
        OpenDialog1.isActive=true
        OpenDialog1.isSaveActive=false
        //toolButton1.action= backed
        toolButton.visible=true

        main_label.text=OpenDialog1.current_path


        textArea.visible=false;
        addImage_button.visible=false
        addsnip_button.visible=false
sendMailButton.visible=false;
        saveButton.visible=false
        newButton.visible=true

        backButton.action=loadDialogBackAction;
        backButton.visible=true



        editButton.visible=false;
        okButton.visible=false;


    }

    Column
    {

        id: menu
        spacing: 20
        width: parent.width


        ColumnLayout
        {
            spacing: 20
            anchors.leftMargin: 0
            width: parent.width

            Layout.alignment :Qt.AlignLeft

            Repeater
            {
                id:r1
                model:OpenDialog1.dir_size//size
                Button
                {
                    Layout.fillWidth: true
                    id:d
                    Material.background: Material.DeepOrange
                    Text
                    {
                        id: d_pic
                        y:5
                        x:5
                        font.pixelSize:32
                        color: "#ffffff"
                        text: qsTr("\ue806")
                    }

                    Text
                    {
                        id: d_text
                        y:14
                        x:5+32+5
                        color: "#ffffff"
                        text: qsTr("\ue808")
                    }


                    Component.onCompleted:
                    {
                        OpenDialog1.dir_iterator=index
                        d_text.text=OpenDialog1.dir_render
                    }

                    onClicked:
                    {
                        OpenDialog1.dir_iterator=index
                        OpenDialog1.current_path="in"
                        stackView.pop()
                        stackView.push("openDialog.qml")
                    }
                }






            }








            Repeater
            {
                id:r2
                model:OpenDialog1.file_size//size
                Button
                {
                    Layout.fillWidth: true
                    id:d2
                    Material.background:  Material.Green
                    Text {
                        id: d2_pic
                        y:5
                        x:5
                        font.pixelSize:32
                        color: "#ffffff"
                        text: qsTr("\uf0f6")
                    }

                    Text {
                        id: d2_text
                        y:14
                        x:5+32+5
                        color: "#ffffff"
                        text: qsTr("\ue808")
                    }


                    Component.onCompleted:
                    {
                        OpenDialog1.file_iterator=index
                        d2_text.text=OpenDialog1.file_render
                    }

                    onClicked:
                    {
                        OpenDialog1.file_iterator=index
                        console.log(OpenDialog1.file)
                        document.load("file:"+OpenDialog1.file)
                        stackView.pop()
                        stackView.push("texteditor.qml")
                    }
                }


            }




        }



    }




    ScrollIndicator.vertical: ScrollIndicator { }

}

