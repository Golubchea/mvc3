//import QtQuick 2.6
//import QtQuick.Layouts 1.0
//import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.12

import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtLocation 5.6
import QtPositioning 5.5
Page {
    width: parent.width
    height: parent.height
    id:home
    title: qsTr("Home")

    Component.onCompleted:
    {

    }

    Image {
        id: background11
        source: "qrc:/images/Bg@3x.png"
        width: parent.width
        height: parent.height
         fillMode: Image.Tile
    }


    Column
    {
        id:main_column
        //anchors.centerIn: parent
        spacing: 10
        x:0
        y:50
        width: parent.width
        height: parent.height


        Image
        {
            x:parent.width/2-50
            y:60
            // horizontalAlignment: Qt.AlignHCenter
            source: "qrc:/images/Logo.png"
        }
    }




    RoundButton
    {
        Material.accent: Material.Red
        anchors.centerIn: parent
        id: button_data_button
        highlighted: true
        text: "Приемка"
        anchors.horizontalCenter: parent.horizontalCenter
        //width: buttonWidth
        onClicked: contentDialog.open()

        Dialog
        {
            id: contentDialog

            x: (parent.width - width) / 2
            y: (parent.height - height) / 2
            width: Math.min(home.width, home.height) / 3 * 2
            contentHeight: parent.height-50
            parent: Overlay.overlay

            modal: true
            title: "Новая квартира"
            standardButtons: Dialog.Yes | Dialog.No
            onAccepted:
            {
                kitchen.visible=home_rooms_kitchen.checked;
                hallway.visible=home_rooms_hallway.checked;
                corridor.visible=home_rooms_corridor.checked;
                bathroom1.visible=home_rooms_bathroom1.checked;
                bathroom2.visible=home_rooms_bathroom2.checked;
                balcony.visible=home_rooms_balcony.checked;
                lodgie.visible=home_rooms_lodgie.checked;
                garage.visible=home_rooms_garage.checked;



                document.set_mail(mail_client.text);

                var num= home_rooms_num.currentValue;


                if(num>room1.idx)
                {
                    room1.visible=true;
                }
                else
                {
                    room1.visible=false;
                }


                if(num>room2.idx)
                {
                    room2.visible=true;
                }
                else
                {
                    room2.visible=false;
                }

                if(num>room3.idx)
                {
                    room3.visible=true;
                }
                else
                {
                    room3.visible=false;
                }

                if(num>room4.idx)
                {
                    room4.visible=true;
                }
                else
                {
                    room4.visible=false;
                }

                if(num>room5.idx)
                {
                    room5.visible=true;
                }
                else
                {
                    room5.visible=false;
                }

                if(num>room6.idx)
                {
                    room6.visible=true;
                }
                else
                {
                    room6.visible=false;
                }


                if(num>room7.idx)
                {
                    room7.visible=true;
                }
                else
                {
                    room7.visible=false;
                }


                if(num>room8.idx)
                {
                    room8.visible=true;
                }
                else
                {
                    room8.visible=false;
                }


                if(num>room9.idx)
                {
                    room9.visible=true;
                }
                else
                {
                    room9.visible=false;
                }




                if(num>room10.idx)
                {
                    room10.visible=true;
                }
                else
                {
                    room10.visible=false;
                }


                if(num>room11.idx)
                {
                    room11.visible=true;
                }
                else
                {
                    room11.visible=false;
                }


                if(num>room12.idx)
                {
                    room12.visible=true;
                }
                else
                {
                    room12.visible=false;
                }


                if(num>room13.idx)
                {
                    room13.visible=true;
                }
                else
                {
                    room13.visible=false;
                }



                if(num>room14.idx)
                {
                    room14.visible=true;
                }
                else
                {
                    room14.visible=false;
                }


                if(num>room15.idx)
                {
                    room15.visible=true;
                }
                else
                {
                    room15.visible=false;
                }




                document.set_m_otdelka(home_rooms_otdelkas.currentText)
                document.set_m_houseNum(house_num.text)
                document.set_m_houseRoomNum(house_num_kv.text)
                document.set_m_checkerName(fio.text)
                document.set_m_address(address.text)



                Data11.select_i=home_rooms_otdelkas.currentIndex


                stackView.push("texteditor.qml")
            }

            Flickable
            {
                id: flickable
                clip: true
                anchors.fill: parent
                contentHeight: column.height

                Column
                {
                    id: column
                    spacing: 20
                    width: parent.width






                    RoundButton
                    {
                        Material.accent: Material.Red
                        anchors.horizontalCenter: parent.horizontalCenter
                        id: button_data_map
                        highlighted: true
                        text: "карта"
                        //anchors.horizontalCenter: parent.horizontalCenter
                        //width: buttonWidth
                        onClicked: mapDialog.open();
                        Dialog
                        {

                            id: mapDialog
                            contentWidth:  home.width
                            contentHeight: home.height
                            x:-150
                            y:-50

                            Map
                            {
                                anchors.fill:parent
                                plugin: Plugin {name: "osm"}
                                center: QtPositioning.coordinate(59.97, 30.32)
                                id: map
                                zoomLevel: 14
                                gesture.enabled: true
                                gesture.acceptedGestures: MapGestureArea.PinchGesture | MapGestureArea.PanGesture | MapGestureArea.TiltGesture
                                signal showPlace(variant coordinate)
                                onZoomLevelChanged:
                                {


                                }


                                //                                PositionSource{
                                //                                    id: positionSource
                                //                                    active: true

                                //                                    onPositionChanged: {
                                //                                        map.center = positionSource.position.coordinate
                                //                                    }
                                //                                }


                                MapQuickItem {
                                    id:marker
                                    sourceItem: Image{
                                        id: image
                                        source: "qrc:/images/marker.png"

                                    }
                                    coordinate: map.center
                                    anchorPoint.x: image.width / 2
                                    anchorPoint.y: image.height / 2
                                }

                                MouseArea
                                {
                                    anchors.fill: parent
                                    id:mmm
                                    onPressed: {
                                        marker.coordinate = map.toCoordinate(Qt.point(mouse.x,mouse.y))
                                        console.log(marker.coordinate);

                                        var coordinate = QtPositioning.coordinate(parseFloat(map.center.latitude ),
                                                                                  parseFloat(map.center.longitude));

                                        var doc = new XMLHttpRequest();
                                        var postData = "data=[out:json];way(around:100,"+marker.coordinate.latitude+", "+marker.coordinate.longitude+")[highway][name];out qt;";
                                        doc.open("POST", "https://lz4.overpass-api.de/api/interpreter", true)
                                        doc.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
                                        doc.send(postData)
                                        doc.onreadystatechange = function()
                                        {
                                            if (doc.readyState === 4) {
                                                console.log("Your data has been sent")
                                                if (doc.status == 200) {console.log("URL exists");
                                                    var root = JSON.parse(doc.response).elements[0].tags.name;

                                                    address.text=root;
                                                    console.log(root)
                                                }}
                                            else console.log("no responce")
                                        }

                                    }
                                }


                                RoundButton
                                {
                                    anchors.horizontalCenter: parent.horizontalCenter
                                    Material.accent: Material.Red
                                    highlighted: true
                                    text: "выбрать"
                                    onClicked: mapDialog.close();
                                }





                            }


                        }


                        //
                    }


                    TextField
                    {
                        id:address
                        width: parent.width
                        focus: true
                        placeholderText: "Адрес"
                        Layout.fillWidth: true
                    }



                    TextField
                    {
                        id: house_num
                        width: parent.width
                        focus: true
                        placeholderText: "Дом"
                        Layout.fillWidth: true
                    }
                    TextField
                    {
                        id: house_num_kv
                        width: parent.width
                        focus: true
                        placeholderText: "Квартира"
                        Layout.fillWidth: true
                    }
                    TextField
                    {
                        id:fio
                        width: parent.width
                        focus: true
                        placeholderText: "ФИО проверяющего"
                        Layout.fillWidth: true
                    }

                    TextField
                    {
                        id:mail_client
                        width: parent.width
                        focus: true
                        placeholderText: "email клиента"
                        Layout.fillWidth: true
                    }


                    Label
                    {
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: "Количество комнат:"
                    }
                    ComboBox {
                        id:home_rooms_num
                        width: parent.width-20
                        model: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14","15"]
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                    ComboBox {
                        id:home_rooms_otdelkas
                        width: parent.width-20
                        model: ["С отделкой", "Без отделки"]
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                    Label
                    {

                        anchors.horizontalCenter: parent.horizontalCenter
                        text: "Cписок помещений:"
                    }
                    CheckBox
                    {
                        checked: true
                        id:home_rooms_kitchen
                        text: "Кухня"
                        Layout.fillWidth: true
                    }
                    CheckBox
                    {
                        checked: true
                        id:home_rooms_hallway
                        text: "Прихожая"
                        Layout.fillWidth: true
                    }
                    CheckBox
                    {
                        checked: true
                        id:home_rooms_corridor
                        text: "Коридор"
                        Layout.fillWidth: true
                    }
                    CheckBox
                    {
                        id:home_rooms_bathroom1
                        text: "С/у"
                        Layout.fillWidth: true
                    }
                    CheckBox
                    {
                        id:home_rooms_bathroom2
                        text: "Ванная"
                        Layout.fillWidth: true
                    }
                    CheckBox
                    {
                        id:home_rooms_balcony
                        text: "Балкон"
                        Layout.fillWidth: true
                    }
                    CheckBox
                    {
                        id:home_rooms_lodgie
                        text: "Лоджия"
                        Layout.fillWidth: true
                    }
                    CheckBox
                    {
                        id:home_rooms_garage
                        text: "Гараж"
                        Layout.fillWidth: true
                    }



















                }











                ScrollIndicator.vertical: ScrollIndicator
                {
                    parent: contentDialog.contentItem
                    anchors.top: flickable.top
                    anchors.bottom: flickable.bottom
                    anchors.right: parent.right
                    anchors.rightMargin: -contentDialog.rightPadding + 1
                }
            }





        }
        Column
        {
            y:main_column.y+20
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 20


            RoundButton
            {
                Material.accent: Material.Red
                // anchors.centerIn: parent
                id: button_photos
                highlighted: true
                text: "Сделать фотоотчет"
                anchors.horizontalCenter: parent.horizontalCenter
                //width: buttonWidth

                onClicked: { stackView.push("Camera2.qml")}
            }

            RoundButton
            {
                Material.accent: Material.Red
                //anchors.centerIn: parent
                id: button_calculator
                highlighted: true
                text: "Калькулятор"
                anchors.horizontalCenter: parent.horizontalCenter
                //width: buttonWidth
                onClicked:
                {
    stackView.push("table/tables.qml")
                }
            }

        }


    }



    //////////////////////




}


