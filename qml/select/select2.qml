
import QtQuick 2.6
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.1

import QtQuick.Controls.Material 2.1
Flickable {
    id: flickable



    Image {
        id: background11
        source: "qrc:/images/Bg@3x.png"
         fillMode: Image.Tile
    }
    contentHeight: menu.height


                Material.accent: "#ed1925"


    Column {
        id: menu
        spacing: 20
        width: parent.width

        Label {
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            color: "white"
            text: "\n"+"Что у квартиры не так?"
        }

        ColumnLayout {
            spacing: 20
            anchors.horizontalCenter: parent.horizontalCenter

            //////////////////

            Repeater
            {
                model:Data11.js//size
                id:lv_2

                RoundButton
                {


                    y: index*50
                    id: button_data2
                    highlighted: true
                    Layout.fillWidth: true
                    visible:
                    {
                        if(button_data2.text==="no i size" ||button_data2.text==="no j size"||button_data2.text==="")
                        {
                            button_data2.visible=false
                        }
                        else
                        {
                             button_data2.visible=true
                        }
                    }

                    // text:Data11.level_1
                    text:
                    {
                        Data11.i=Data11.select_i
                        Data11.j=index//*it
                        button_data2.text=Data11.level_2

                    }


                    onClicked:
                    {
                        Data11.select_j=index
                        stackView.push("select3.qml")
                    }
                }
            }






             RoundButton {
                text: "Назад"
                Layout.fillWidth: true
                 Material.background: "white"
                onClicked: {
                    stackView.pop() }
            }
        }
    }


    ScrollIndicator.vertical: ScrollIndicator { }

}
