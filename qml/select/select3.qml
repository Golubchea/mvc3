


import QtQuick 2.6
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.1

import QtQuick.Controls.Material 2.1
Flickable {
    id: flickable

    contentHeight: menu.height

    Image {
        id: background11
        source: "qrc:/images/Bg@3x.png"
         fillMode: Image.Tile
    }

    Material.accent: "green"


    Column {
        id: menu
        spacing: 20
        width: parent.width

        Label {
            width: parent.width
            wrapMode: Label.Wrap
            horizontalAlignment: Qt.AlignHCenter
            color: "white"
            text: "\n"+"Строительные нормы и правила"
        }

        ColumnLayout {
            spacing: 20
            anchors.horizontalCenter: parent.horizontalCenter

            //////////////////

            Repeater
            {
                model:Data11.ks//size
                id:lv_3

                RoundButton
                {


                    y: index*50
                    id: button_data3
                    highlighted: true
                    Layout.fillWidth: true
                    Material.accent: Material.Red
                    visible:
                    {
                        if(button_data3.text==="no i size" ||button_data3.text==="no j size"||button_data3.text==="")
                        {
                            button_data3.visible=false
                        }
                        else
                        {
                            button_data3.visible=true
                        }
                    }

                    // text:Data11.level_1
                    text:
                    {
                        Data11.i=Data11.select_i
                        Data11.j=Data11.select_j
                        Data11.k=index//*it
                        button_data3.text=Data11.level_3

                    }


                    onClicked:
                    {
                        Data11.select_k=index
                        stackView.pop()
                        stackView.pop()
                        stackView.pop()
                        stackView.push("../texteditor.qml")
                         document.attachText(Data11.res_string)


                        textArea.readOnly = false
                        // Force focus on the text area so the cursor and footer show up.
                        textArea.forceActiveFocus()
                        //textArea.persistentSelection=false
                        editButton.visible=false;
                        okButton.visible=true;


                        addImage_button.visible=true

                        addsnip_button.visible=true


                    }
                }
            }



             RoundButton {
                text: "Назад"
                Layout.fillWidth: true
                 Material.background: "white"
                onClicked: {
                    stackView.pop() }
            }
        }
    }


    ScrollIndicator.vertical: ScrollIndicator { }

}
