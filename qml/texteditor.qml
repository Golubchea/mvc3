﻿
//import QtQuick 2.6
//import QtQuick.Layouts 1.0
//import QtQuick.Controls 2.1
//import QtQuick.Controls.Material 2.1




import QtQuick 2.12

import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import QtQuick.Layouts 1.12
import QtQuick.Window 2.0
import Qt.labs.platform 1.0
import texteditor 1.0

Page{
    Slider {
        x:window.width-40
        y:0
        id :sl
        height: window.height-40
        //height: window.height-60
        //width: window.width-60
        orientation: Qt.Vertical
        Material.accent: Material.Red
        from:  1000
        value:textArea.cursorPosition
        to:0
        //anchors.horizontalCenter: parent.horizontalCenter
        onMoved:
        {
            textArea.select(sl.value,sl.value+1)
        }



    }


    Flickable {
        id: textEditor
        //contentHeight: parent.height
        TextArea.flickable: textArea

        height: window.height-10
        width: window.width-20






        ScrollIndicator.vertical: ScrollIndicator
        {
            id:sc
//            parent: textEditor.parent
//                   anchors.top: textEditor.top
//                   anchors.left: textEditor.right
//                   anchors.bottom: textEditor.bottom


        }



        Action {
            id: imageAttachAction
            text: qsTr("&imageDialog")
            icon.name: "imageDialog"
            //on
            onTriggered:{

                // Data11.image1="/home/u/Изображения/IMG_00000001.jpg"
                //  document.addImage(Data11.image1)
                document.cursorPosition=scroll.value

                stackView.push("qrc:/qml/Camera.qml")
                //document.attachImage(Data11.image1)
            }

        }


        Action {
            id: dataAttachAction
            text: qsTr("&imageDialog")
            icon.name: "imageDialog"
            //on
            onTriggered:
            {
                // Q_PROPERTY(int selectionStart READ selectionStart WRITE setSelectionStart NOTIFY selectionStartChanged)
                // Q_PROPERTY(int selectionEnd READ selectionEnd WRITE setSelectionEnd NOTIFY selectionEndChanged)
                document.cursorPosition=scroll.value
                stackView.push("select/select2.qml")

            }

        }




        Action {
            id: saveAsAction
            text: qsTr("&saveDialog")
            icon.name: "saveDialog"
            //on
            onTriggered:
            {
                document.fileTypeID=2
                //document.saveAs("file://home/u/Документы/Houses/вавам/dfs.txt")

                console.log("file:/"+OpenDialog1.current_path+"/"+document.getAddr()+"."+"odt")
                document.saveAs("file:/"+OpenDialog1.current_path+"/"+document.getAddr()+"."+"odt")
                //file_name.text=  document.fileName
            }


        }


        Component.onCompleted:
        {


            m.visible=true;

            textArea.visible=true;
            toolButton.visible=true;
            main_label.text=document.fileName

sendMailButton.visible=true;
            saveButton.visible=true;
            newButton.visible=false;
            backButton.visible=false;

            addImage_button.action=imageAttachAction
            // addImage_button.visible=true
            addsnip_button.action=dataAttachAction
            // addsnip_button.visible=true

            saveButton.action=saveAsAction;
            editButton.visible=true
            okButton.visible=false
            textArea.readOnly = true
            //editButton.visible=true
            // Force focus on the text area so the cursor and footer show up.


            // textArea.forceActiveFocus()
        }







    }









}






